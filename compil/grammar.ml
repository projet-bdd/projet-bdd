open Parser 

(* Définition de la grammaire *)

let requestNT = "REQUEST";;
let request = [NT("REQUEST")];;

let selectionNT = "SELECTION"
let selection = [NT("SELECTION")];;

let tableOperationNT = "TABLE_OPERATION";;
let tableOperation = [NT(tableOperationNT)];;

let tableNT = "TABLE";;
let table = [NT("TABLE")];;

let attributNT = "ATTRIBUT"
let attribut = [NT("ATTRIBUT")];;

let indexNT = "INDEX";;
let index = [NT("INDEX")];;

let logiqueNT = "LOGIQUE";;
let logique = [NT("LOGIQUE")];;

let logandNT = "AND";;
let logand = [NT("AND")];;

let egalNT = "EGAL";;
let egal = [NT("EGAL")];;

let renameTableNT = "RENAME_TABLE"
let renameTable = [NT(renameTableNT)];;

let renameAttributNT = "RENAME_ATTRIBUT"
let renameAttribut = [NT(renameAttributNT)];;


let sp = [T(' ')];;
let opar = [T('(')];;
let cpar = [T(')')];;
let eq = [T('=')];;
let neq = [T('!'); T('=')];;
let vir = [T(',')];;
let point = [T('.')];;


let to_term s = String.fold_right (fun c acc -> T(c) :: acc) s [];;
let select = [T('s'); T('e'); T('l'); T('e'); T('c'); T('t')];;
let from = [T('f'); T('r'); T('o'); T('m')];;
let where = [T('w'); T('h'); T('e'); T('r'); T('e')];;
let join = [T('j'); T('o'); T('i'); T('n')];;
let on = [T('o'); T('n')];;
let alias = [T('a'); T('s')];;
let loand = [T('a'); T('n'); T('d')];;
let loor = [T('o'); T('r')];;
let union = to_term "union";;
let diff = to_term "diff";;
let cross = to_term "cross";;

let rules =  [
  (requestNT, select @ sp @ selection @ sp @ from @ sp @ tableOperation @ sp @ where @ sp @ logique);
  (requestNT, select @ sp @ selection @ sp @ from @ sp @ tableOperation);

  (selectionNT, renameAttribut @ vir @ sp @ selection);
  (selectionNT, index @ vir @ sp @ selection);
  (selectionNT, renameAttribut);
  (selectionNT, index);

  (tableOperationNT, opar @ tableOperation @ cpar);
  (tableOperationNT, tableOperation @ sp @ join @ sp @ tableOperation @ sp @ on @ sp @ logique);
  (tableOperationNT, tableOperation @ sp @ union @ sp @ tableOperation);
  (tableOperationNT, tableOperation @ sp @ diff @ sp @ tableOperation);
  (tableOperationNT, tableOperation @ sp @ cross @ sp @ tableOperation);
  (tableOperationNT, request);
  (tableOperationNT, renameTable);
  (tableOperationNT, table);

  (tableNT, littword);
  (attributNT, littword);
  (indexNT, table @ point @ attribut);
  (renameAttributNT, index @ sp @ alias @ sp @ attribut);
  (renameTableNT, tableOperation @ sp @ alias @ sp @ table);

  (logiqueNT, opar @ logique @ cpar);
  (logiqueNT, logique @ sp @ loand @ sp @ logique);
  (logiqueNT, index @ sp @ eq @ sp @ index);
  (logiqueNT, index @ sp @ neq @ sp @ index);
  (logiqueNT, index @ sp @ eq @ sp @ number);
  (logiqueNT, index @ sp @ neq @ sp @ number);
  
];;

let (_, rules_numbers) = grammar_numbers;;
let (_, rules_littwords) = grammar_littwords;;
let grammar_sql = (requestNT, rules @ rules_numbers @ rules_littwords);;
let parse_sql = parse grammar_sql


(* let () = match parse_sql "select alpha.beta as toto, beta.alpha from (alpha join isabelle on isabelle.k = alpha.z) as table join lilou on lilou.a = 214 where alpha.delta = 100" with
  | None -> print_string "Echec"
  | Some(t) -> print_stree t;; *)
