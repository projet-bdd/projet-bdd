open Parser
open Grammar

type table = string
type index = string
type attribut = table*index
type rename = (attribut*attribut) list

type formula =
  | Eq of attribut * attribut
  | And of formula * formula
  | Or of formula * formula

type query =
  | RELATION of table
  | RENAME of query * rename
  | RENAME_TABLE of query * table
  | PROJ of query * attribut list
  | SELECT of query * formula
  | CROSS of query * query 
  | UNION of query * query 
  | DIFF of query * query
  | JOIN of query * query * formula


(* let rec print_formula f =  *)
(*   match f with  *)
(*   | Eq ((x, y), (a, b)) -> print_string x; print_string ".";print_string y; print_string " = "; print_string a; print_string "."; print_string b; *)
(*   | And(f1, f2) -> print_formula f1; print_string " and "; print_formula f2 *)
(*   | Or(f1, f2) -> print_formula f1; print_string " or "; print_formula f2 *)

(* let rec print_querry q = *)
(*   match q with *)
(*   | RELATION(t) -> print_string t *)
(*   | RENAME(q', r) -> ( *)
(*     print_string "RENAME"; *)
(*     List.iter (fun ((x,y), (a,b)) -> print_string "{";print_string x; print_string ".";print_string y; print_string "->"; print_string a; print_string "."; print_string b;print_string "}") r; *)
(*     print_string "("; *)
(*     print_querry q'; *)
(*     print_string ")"; *)
(*   ) *)
(*   | RENAME_TABLE(q', new_name) -> ( *)
(*     print_string "RENAME_TABLE{"; *)
(*     print_string new_name; *)
(*     print_string "}("; *)
(*     print_querry q'; *)
(*     print_string ")"; *)
(*   ) *)
(*   | PROJ(q', sel) -> ( *)
(*     print_string "PROJ"; *)
(*     List.iter (fun (x,y) -> print_string "{";print_string x; print_string ".";print_string y; print_string "}") sel; *)
(*     print_string "("; *)
(*     print_querry q'; *)
(*     print_string ")"; *)
(*   ) *)
(*   | SELECT(q', f) -> ( *)
(*     print_string "SELECT{"; *)
(*     print_formula f; *)
(*     print_string "}("; *)
(*     print_querry q'; *)
(*     print_string ")"; *)
(*   ) *)
(*   | JOIN(q1, q2, f) -> ( *)
(*     print_string "("; *)
(*     print_querry q1; *)
(*     print_string ")"; *)
(*     print_string "JOIN{"; *)
(*     print_formula f; *)
(*     print_string "}("; *)
(*     print_querry q2; *)
(*     print_string ")"; *)
(*   ) *)
(*   | CROSS(q1, q2) -> ( *)
(*     print_string "("; *)
(*     print_querry q1; *)
(*     print_string ") x ("; *)
(*     print_querry q2; *)
(*     print_string ")"; *)
(*   ) *)
(*   | UNION(q1, q2) -> ( *)
(*     print_string "("; *)
(*     print_querry q1; *)
(*     print_string ") U ("; *)
(*     print_querry q2; *)
(*     print_string ")"; *)
(*   ) *)
(*   | DIFF(q1, q2) -> ( *)
(*     print_string "("; *)
(*     print_querry q1; *)
(*     print_string ") \\ ("; *)
(*     print_querry q2; *)
(*     print_string ")"; *)
(*   ) *)




let rec to_attribut (t : stree) : attribut*(attribut option) = 
  match t with
  | N(name, lst) when name=indexNT -> (
    match lst with
    | [N(_, [LittWord(tbl)]); _; N(_, [LittWord(idx)])] -> ((tbl, idx), None)
    | _ -> failwith "impossible"
  )
  | N(name, lst) when name=renameAttributNT -> (
    match lst with
    | [attr; _; _;_; _; N(_, [LittWord(nidx)])] -> (
      let ((tbl, idx), _) = to_attribut attr in
      ((tbl, idx), Some((tbl, nidx)))
    )
    | _ -> failwith "impossible"
  )
  | _ -> failwith "unreachable"

let rec to_query (t : stree) =
  match t with
  | N(name, lst) -> (
    if name = requestNT then (
      match lst with
      (* s e l e c t                     f r o m                       w h e r e            *)
      | [_;_;_;_;_;_; (* *)_; sel; (* *)_; _;_;_;_; (* *)_; tblOp; (* *)_; _;_;_;_;_; (* *)_; log] -> (
        let (attr_lst, rename_lst) = to_attr_lst sel in
        if rename_lst <> [] then RENAME(PROJ(SELECT(to_query tblOp, to_formula log), attr_lst), rename_lst)
        else PROJ(SELECT(to_query tblOp, to_formula log), attr_lst)
      )
      (* s e l e c t                     f r o m                *)
      | [_;_;_;_;_;_; (* *)_; sel; (* *)_; _;_;_;_; (* *)_; tblOp] -> (
        let (attr_lst, rename_lst) = to_attr_lst sel in
        if rename_lst <> [] then RENAME(PROJ(to_query tblOp, attr_lst), rename_lst)
        else PROJ(to_query tblOp, attr_lst)
      )
      | _ -> failwith "Impossible"
    )
    else if name = tableOperationNT then (
      match lst with
      (* (      ) *)
      | [_;subt;_] -> to_query subt
      (*            j o i n                o n        *)
      | [tblop1; _; _;_;_;_; _; tblop2; _; _;_; _; log] -> (
        JOIN(to_query tblop1, to_query tblop2, to_formula log)
      )
      (*            u n i o n                *)
      | [tblop1; _; C('u');_;_;_;_; _; tblop2] -> (
          UNION(to_query tblop1, to_query tblop2)
        )
      (*            d i f f *)
      | [tblop1; _; _;_;_;_; _; tblop2] -> (
          DIFF(to_query tblop1, to_query tblop2)
        )
      (*            c r o s s *)
      | [tblop1; _; C('c');_;_;_;_; _; tblop2] -> (
          CROSS(to_query tblop1, to_query tblop2)
        )
      | [elt] -> (
        match elt with
        | N(name, _) when name = requestNT -> to_query elt
        | N(name, [subtblop; _; _;_; _; N(_, [LittWord(t)])]) when name = renameTableNT -> RENAME_TABLE(to_query subtblop, t)
        | N(name, [LittWord(t)]) when name = tableNT -> RELATION(t)
        | _ -> failwith "Impossible"
      )
      | _ -> failwith "unreachable"
    )
    else (
      failwith "Impossible"
    )
  )

  | _ -> failwith "Impossible"

and to_attr_lst (t: stree) : attribut list * rename = 
  match t with
  | N(name, lst) when name=selectionNT -> (
    match lst with
    | [elt; _; _; next] -> (
      let (attr_lst, rename_lst) = to_attr_lst next in
      let (attr, new_attr) = to_attribut elt in
      match new_attr with
      | None -> (attr::attr_lst, rename_lst)
      | Some(new_attr) -> (attr::attr_lst, (attr, new_attr)::rename_lst)
    )
    | [elt] -> (
      let (attr, new_attr) = to_attribut elt in
      match new_attr with
      | None -> ([attr], [])
      | Some(new_attr) -> ([attr], [(attr, new_attr)])
    )
    | _ -> failwith "unreachable"
  )
  | _ -> failwith "unreachable"

and to_formula (t: stree) =
  match t with
  | N(name, lst) -> (
    if name = logiqueNT then (
      match lst with
      (* (      ) *)
      | [_;subt;_] -> to_formula subt
      (*        a n d      *)
      | [t1; _; _;_;_; _; t2] -> (
        And(to_formula t1, to_formula t2)
      )
      (*        =       *)
      | [t1; _; _; _; t2] -> (
        match t2 with
        (* Cas index = index *)
        | N(_, _) -> (
          let (attr1, _) = to_attribut t1 in
          let (attr2, _) = to_attribut t2 in
          Eq(attr1, attr2)
        )
        | _ -> failwith "Equality with an int is not implemented yet"
      )
      | _ -> failwith "Further logical formula is not implemented"
    )
    else (
      failwith "Impossible"
    )
  )
  | _ -> failwith "unreachable"

let compile (str : string) : query option = 
  match parse_sql str with
  | None -> None
  | Some(q) -> Some(to_query q)

