type word = char list (* Un mot classique sous forme de liste de caractères *)

type gchar = 
| T of char
| NT of string
type gword = gchar list

type rule = string*gword
type grammar = (string * (rule list))

type stree =
| C of char
| N of string*(stree list)
| Int of int
| LittWord of string

let rec print_stree (t : stree) =
  match t with
  | C(a) -> print_char a;
  | N(nom, args) -> (
    print_string nom;
    print_string "(";
    let rec aux args =
      match args with 
      | [] -> ()
      | [x] -> print_stree x
      | x::xs -> print_stree x; print_string ", "; aux xs;
    in aux args;
    print_string ")"
  )
  | Int(n) -> print_int n
  | LittWord(str) -> print_string str
  ;;

(* Fonctions utilitaires *)

let word_of_string (str : string) =
  let res = ref [] in
  for i = (String.length str)-1 downto 0 do
    res := String.get str i :: !res;
  done;
  !res

let rec rec_parse (rules : rule list) ((nt, w) : rule) (txt : word) : stree option =
  match w, txt with
  | [], [] -> (let res = N(nt, []) in (*print_stree res; print_newline ();*) Some(res))
  | _::_, [] -> None
  | [], _::_ -> None
  | T(a)::w', b::txt' -> (
    if a = b then (
      match rec_parse rules (nt, w') txt' with
      | Some(N(_, lst)) -> (let res = N(nt, C(a)::lst) in (*print_stree res; print_newline ();*) Some(res))
      | _ -> None
    )
    else None
  )
  | NT(nt')::w', _ -> (
    let rec cut_txt (prefix : word) (postfix : word) = 
      let topt = rec_parse rules (nt, w') postfix in
      match topt with
      | Some(N(_, lst)) -> (
        let rec test_rules (rules' : rule list) = 
          match rules' with
          | (nt'', w'')::rules'' when nt'' = nt' -> (
            let topt' = rec_parse rules (nt'', w'') prefix in
            match topt' with
            | Some(t) -> (let res = N(nt, t::lst) in (*print_stree res; print_newline ();*) Some(res))
            | None -> test_rules rules''
          )
          | _::rules'' -> test_rules rules''
          | [] -> None
        in match test_rules rules with
        | Some(t) -> Some(t)
        | None -> match postfix with | [] -> None | x::xs -> cut_txt (prefix @ [x]) xs
      )
      | _ -> match postfix with | [] -> None | x::xs -> cut_txt (prefix @ [x]) xs
    in cut_txt [] txt
  )

(* Grammaires prédéfinies *)

let gram_number = [
  ("NUMBER", [T('0'); NT("NUMBER")]);
  ("NUMBER", [T('1'); NT("NUMBER")]);
  ("NUMBER", [T('2'); NT("NUMBER")]);
  ("NUMBER", [T('3'); NT("NUMBER")]);
  ("NUMBER", [T('4'); NT("NUMBER")]);
  ("NUMBER", [T('5'); NT("NUMBER")]);
  ("NUMBER", [T('6'); NT("NUMBER")]);
  ("NUMBER", [T('7'); NT("NUMBER")]);
  ("NUMBER", [T('8'); NT("NUMBER")]);
  ("NUMBER", [T('9'); NT("NUMBER")]);
  ("NUMBER", [T('0')]);
  ("NUMBER", [T('1')]);
  ("NUMBER", [T('2')]);
  ("NUMBER", [T('3')]);
  ("NUMBER", [T('4')]);
  ("NUMBER", [T('5')]);
  ("NUMBER", [T('6')]);
  ("NUMBER", [T('7')]);
  ("NUMBER", [T('8')]);
  ("NUMBER", [T('9')]);
];;
let grammar_numbers = ("NUMBER", gram_number);;
let number = [NT("NUMBER")];;
let numberNT = "NUMBER";;

let int_of_char c = (Char.code c) - (Char.code '0')

let rec get_number (t : stree) : (int*int) option =
  match t with
  | N(nt, lst) when nt = "NUMBER" -> (
    match lst with
    | [] -> Some(0, 1)
    | [C(c)] -> Some(int_of_char c, 10)
    | [x; y] -> (
      match x, (get_number y) with
      | C(n), Some(m, k) -> Some((int_of_char n)*k+m, k*10)
      | _, _ -> None
    )
    | _ -> None
  ) 
  | _ -> None

let rec reduce_grammar_numbers (t : stree) : stree =
  match t with
  | N(str, lst) -> (
    let rec iter lst =
      match lst with
      | [] -> []
      | t'::ts -> (
        match get_number t' with
        | Some(n, _) -> Int(n)::iter ts
        | None -> reduce_grammar_numbers t'::iter ts
      )
    in N(str, iter lst)
  )
  | _ -> t






let gram_str = [
  ("STRING", [T('a'); NT("STRING")]);
  ("STRING", [T('b'); NT("STRING")]);
  ("STRING", [T('c'); NT("STRING")]);
  ("STRING", [T('d'); NT("STRING")]);
  ("STRING", [T('e'); NT("STRING")]);
  ("STRING", [T('f'); NT("STRING")]);
  ("STRING", [T('g'); NT("STRING")]);
  ("STRING", [T('h'); NT("STRING")]);
  ("STRING", [T('i'); NT("STRING")]);
  ("STRING", [T('j'); NT("STRING")]);
  ("STRING", [T('k'); NT("STRING")]);
  ("STRING", [T('l'); NT("STRING")]);
  ("STRING", [T('m'); NT("STRING")]);
  ("STRING", [T('n'); NT("STRING")]);
  ("STRING", [T('o'); NT("STRING")]);
  ("STRING", [T('p'); NT("STRING")]);
  ("STRING", [T('q'); NT("STRING")]);
  ("STRING", [T('r'); NT("STRING")]);
  ("STRING", [T('s'); NT("STRING")]);
  ("STRING", [T('t'); NT("STRING")]);
  ("STRING", [T('u'); NT("STRING")]);
  ("STRING", [T('v'); NT("STRING")]);
  ("STRING", [T('w'); NT("STRING")]);
  ("STRING", [T('x'); NT("STRING")]);
  ("STRING", [T('y'); NT("STRING")]);
  ("STRING", [T('z'); NT("STRING")]);
  ("STRING", [T('A'); NT("STRING")]);
  ("STRING", [T('B'); NT("STRING")]);
  ("STRING", [T('C'); NT("STRING")]);
  ("STRING", [T('D'); NT("STRING")]);
  ("STRING", [T('E'); NT("STRING")]);
  ("STRING", [T('F'); NT("STRING")]);
  ("STRING", [T('G'); NT("STRING")]);
  ("STRING", [T('H'); NT("STRING")]);
  ("STRING", [T('I'); NT("STRING")]);
  ("STRING", [T('J'); NT("STRING")]);
  ("STRING", [T('K'); NT("STRING")]);
  ("STRING", [T('L'); NT("STRING")]);
  ("STRING", [T('M'); NT("STRING")]);
  ("STRING", [T('N'); NT("STRING")]);
  ("STRING", [T('O'); NT("STRING")]);
  ("STRING", [T('P'); NT("STRING")]);
  ("STRING", [T('Q'); NT("STRING")]);
  ("STRING", [T('R'); NT("STRING")]);
  ("STRING", [T('S'); NT("STRING")]);
  ("STRING", [T('T'); NT("STRING")]);
  ("STRING", [T('U'); NT("STRING")]);
  ("STRING", [T('V'); NT("STRING")]);
  ("STRING", [T('W'); NT("STRING")]);
  ("STRING", [T('X'); NT("STRING")]);
  ("STRING", [T('Y'); NT("STRING")]);
  ("STRING", [T('Z'); NT("STRING")]);
  ("STRING", [T('a')]);
  ("STRING", [T('b')]);
  ("STRING", [T('c')]);
  ("STRING", [T('d')]);
  ("STRING", [T('e')]);
  ("STRING", [T('f')]);
  ("STRING", [T('g')]);
  ("STRING", [T('h')]);
  ("STRING", [T('i')]);
  ("STRING", [T('j')]);
  ("STRING", [T('k')]);
  ("STRING", [T('l')]);
  ("STRING", [T('m')]);
  ("STRING", [T('n')]);
  ("STRING", [T('o')]);
  ("STRING", [T('p')]);
  ("STRING", [T('q')]);
  ("STRING", [T('r')]);
  ("STRING", [T('s')]);
  ("STRING", [T('t')]);
  ("STRING", [T('u')]);
  ("STRING", [T('v')]);
  ("STRING", [T('w')]);
  ("STRING", [T('x')]);
  ("STRING", [T('y')]);
  ("STRING", [T('z')]);
  ("STRING", [T('A')]);
  ("STRING", [T('B')]);
  ("STRING", [T('C')]);
  ("STRING", [T('D')]);
  ("STRING", [T('E')]);
  ("STRING", [T('F')]);
  ("STRING", [T('G')]);
  ("STRING", [T('H')]);
  ("STRING", [T('I')]);
  ("STRING", [T('J')]);
  ("STRING", [T('K')]);
  ("STRING", [T('L')]);
  ("STRING", [T('M')]);
  ("STRING", [T('N')]);
  ("STRING", [T('O')]);
  ("STRING", [T('P')]);
  ("STRING", [T('Q')]);
  ("STRING", [T('R')]);
  ("STRING", [T('S')]);
  ("STRING", [T('T')]);
  ("STRING", [T('U')]);
  ("STRING", [T('V')]);
  ("STRING", [T('W')]);
  ("STRING", [T('X')]);
  ("STRING", [T('Y')]);
  ("STRING", [T('Z')]);
];;
let grammar_littwords = ("STRING", gram_str);;
let littword = [NT("STRING")];;
let littwordNT = "STRING";;

let rec get_littword (t : stree) : string option =
  match t with
  | N(nt, lst) when nt = "STRING" -> (
    match lst with
    | [] -> Some("")
    | [C(c)] -> Some(String.make 1 c)
    | [C(c); y] -> (
      match (get_littword y) with
      | Some(str) -> Some((String.make 1 c) ^ str)
      | _ -> None
    )
    | _ -> None
  ) 
  | _ -> None

let rec reduce_grammar_littwords (t : stree) : stree =
  match t with
  | N(str, lst) -> (
    let rec iter lst =
      match lst with
      | [] -> []
      | t'::ts -> (
        match get_littword t' with
        | Some(str) -> LittWord(str)::iter ts
        | None -> reduce_grammar_littwords t'::iter ts
      )
    in N(str, iter lst)
  )
  | _ -> t


(* Fonctions de l'interface *)

let parse ((vdep, gram) : grammar) (txt : string) =
  let to_parse = word_of_string txt in
  let rec tests (rules_to_test : rule list) (iter : int) =
    match rules_to_test with
    | [] -> None
    | (nt, w)::rules' when nt = vdep -> (
      match rec_parse gram (nt, w) to_parse with
      | None -> tests rules' (iter+1)
      | Some(t) -> Some(reduce_grammar_littwords (reduce_grammar_numbers t))
    )
    | _::rules' -> tests rules' (iter+1)
  in tests gram 0

(* let parse ((vdep, gram) : grammar) (txt : string) = rec_parse gram ("", [NT(vdep)]) (word_of_string txt) *)
