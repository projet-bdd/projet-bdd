type table = string
type index = string
type attribut = table*index
type rename = (attribut*attribut) list

type formula =
  | Eq of attribut * attribut
  | And of formula * formula
  | Or of formula * formula

type query =
  | RELATION of table
  | RENAME of query * rename
  | RENAME_TABLE of query * table
  | PROJ of query * attribut list
  | SELECT of query * formula
  | CROSS of query * query 
  | UNION of query * query 
  | DIFF of query * query
  | JOIN of query * query * formula

val compile : string -> query option


(* module type IDENT = sig
  type t
  val eq : t -> t -> bool
end

module type TABLE = sig
  module ID : IDENT
  type t
end

module type ATTRIBUT = sig
  module ID : IDENT
  module TABLE : TABLE
  type attribut
  val id : attribut -> ID.t
  val from : attribut -> TABLE.ID.t
  val eq : attribut -> attribut -> bool
  val rename : attribut -> ID.t -> attribut
end

type attribut = | TODO
type table = | TODO *)

(* ----- module du groupe algo ----- *)
(* type algo_type =
  | JOIN
  | SELECT
  | UNION
  | Autre


module type ALGO = sig
  type meta_inf
  type algo 
  val typ : algo -> algo_type
  val iter : algo_type -> unit -> algo
  val complexity : algo -> meta_inf -> int



end  



module ChoixAlgo(Algo: ALGO) = struct
  let choix : query -> Algo.algo =
    match query with
      | SELECT _ -> Algo.iter SELECT ()
      | JOIN _ -> Algo.iter JOIN ()
      | UNION _ -> Algo.iter UNION ()
      | _ -> Algo.iter Autre ()
end

 *)
