open Index
open Data
open Sort
open Algo_don
(* open Algo_don *)


let create_table1 () =
  let att = [create_attribute_type "id" TINTEGER [PRIMARY_KEY]; create_attribute_type "username" (TVARCHAR 16) []; create_attribute_type "score" TREAL []] in
  create_table "users1" att

let create_table2 () =
  let att = [create_attribute_type "id" TINTEGER [PRIMARY_KEY]; create_attribute_type "username" (TVARCHAR 16) []; create_attribute_type "score" TREAL []] in
  create_table "users2" att

let create_table3 () =
  let att = [create_attribute_type "username" (TVARCHAR 16) []; create_attribute_type "score" TREAL []; create_attribute_type "lateid" TINTEGER [PRIMARY_KEY]] in
  create_table "users3" att



let fill_table1 () = 
  let tbl = get_table "users1" in
  insert_row tbl [|integer 1l; varchar 16 "toto"; real 12.5|];
  insert_row tbl [|integer 2l; varchar 16 "titi"; real 15.0|];
  insert_row tbl [|integer 3l; varchar 16 "tutu"; real 10.0|];
  insert_row tbl [|integer 4l; varchar 16 "tata"; real 20.0|];
  insert_row tbl [|integer 5l; varchar 16 "tete"; real 17.5|];
  insert_row tbl [|integer 6l; varchar 16 "jean"; real 15.0|];
  insert_row tbl [|integer 7l; varchar 16 "paul"; real 10.0|];
  insert_row tbl [|integer 8l; varchar 16 "jacques"; real 20.0|];
  insert_row tbl [|integer 9l; varchar 16 "pierre"; real 17.5|];
  insert_row tbl [|integer 10l; varchar 16 "henri"; real 15.0|]

let fill_table2 () = 
  let tbl = get_table "users2" in
  insert_row tbl [|integer 7l; varchar 16 "paul"; real 10.0|];
  insert_row tbl [|integer 8l; varchar 16 "jacques"; real 20.0|];
  insert_row tbl [|integer 9l; varchar 16 "pierre"; real 17.5|];
  insert_row tbl [|integer 10l; varchar 16 "henri"; real 15.0|];
  insert_row tbl [|integer 11l; varchar 16 "michel"; real 10.0|];
  insert_row tbl [|integer 12l; varchar 16 "robert"; real 20.0|];
  insert_row tbl [|integer 13l; varchar 16 "bernard"; real 17.5|];
  insert_row tbl [|integer 14l; varchar 16 "francois"; real 15.0|];
  insert_row tbl [|integer 15l; varchar 16 "alain"; real 10.0|];
  insert_row tbl [|integer 16l; varchar 16 "olivier"; real 20.0|];
  insert_row tbl [|integer 17l; varchar 16 "thomas"; real 17.5|];
  insert_row tbl [|integer 18l; varchar 16 "nicolas"; real 15.0|];
  insert_row tbl [|integer 19l; varchar 16 "jerome"; real 10.0|];
  insert_row tbl [|integer 20l; varchar 16 "raphael"; real 20.0|];
  insert_row tbl [|integer 21l; varchar 16 "gabriel"; real 17.5|];
  insert_row tbl [|integer 22l; varchar 16 "emmanuel"; real 15.0|];
  insert_row tbl [|integer 23l; varchar 16 "jean-michel"; real 10.0|];
  insert_row tbl [|integer 24l; varchar 16 "jean-pierre"; real 20.0|];
  insert_row tbl [|integer 25l; varchar 16 "jean-henri"; real 17.5|];
  insert_row tbl [|integer 26l; varchar 16 "jean-marie"; real 15.0|];
  insert_row tbl [|integer 27l; varchar 16 "jean-louis"; real 10.0|];
  insert_row tbl [|integer 28l; varchar 16 "jean-jacques"; real 20.0|];
  insert_row tbl [|integer 29l; varchar 16 "jean-paul"; real 17.5|];
  insert_row tbl [|integer 30l; varchar 16 "jean-claude"; real 15.0|]

let fill_table3 () =
  let tbl = get_table "users3" in
  insert_row tbl [|varchar 16 "jean-claude"; real 15.0; integer 30l|];
  insert_row tbl [|varchar 16 "jean-philippe"; real 10.0; integer 31l|];
  insert_row tbl [|varchar 16 "jean-francois"; real 20.0; integer 32l|];
  insert_row tbl [|varchar 16 "jean-bernard"; real 17.5; integer 33l|];
  insert_row tbl [|varchar 16 "jean-robert"; real 15.0; integer 34l|];
  insert_row tbl [|varchar 16 "jean-olivier"; real 20.0; integer 36l|];
  insert_row tbl [|varchar 16 "jean-thomas"; real 17.5; integer 37l|];
  insert_row tbl [|varchar 16 "jean-nicolas"; real 15.0; integer 38l|];
  insert_row tbl [|varchar 16 "jean-jerome"; real 10.0; integer 39l|];
  insert_row tbl [|varchar 16 "jean-raphael"; real 20.0; integer 40l|];
  insert_row tbl [|varchar 16 "jean-gabriel"; real 17.5; integer 41l|];
  insert_row tbl [|varchar 16 "jean-emmanuel"; real 15.0; integer 42l|];
  insert_row tbl [|varchar 16 "jean-jean"; real 10.0; integer 43l|]

let algo () = 
  Printf.printf "Tests\n";
  Printf.printf "\nCréation des tables\n";
  let t1 = get_table "users1" in
  let t2 = get_table "users2" in
  let t3 = get_table "users3" in
  print_table t1;
  print_table t2;
  print_table t3;
  let t4 = block_nested_loop t1 t2 [(List.hd (get_table_attributes t1))] [(List.hd (get_table_attributes t2))] in
  Printf.printf "\nBlock nested loop:\n";
  print_table t4;
  let t5 = union t1 t2 in
  let t6 = distinct t5 in
  let t7 = prod_cart t1 t2 in
  Printf.printf "\nUnion (users1 et users2):\n";
  print_table t5;
  Printf.printf "\nDistinct (résultat précédent):\n";
  print_table t6;
  Printf.printf "\nProduit cartésien (users1 et users2):\n";
  print_table t7;;




let () =
  if not (Sys.file_exists "users1") then (create_table1 (); fill_table1 ());
  if not (Sys.file_exists "users2") then (create_table2 (); fill_table2 ());
  if not (Sys.file_exists "users3") then (create_table3 (); fill_table3 ());
  algo();;

