open Data
open Index


let equal_array a1 a2 =
  let b = ref true in
  let n1 = Array.length a1 in
  let n2 = Array.length a2 in 
  if n1 != n2 then b:=false
  else
    for i=0 to (n1-1) do 
      if a1.(i) != a2.(i) then b:=false
    done;
    !b;;

let prod_cart t1 t2 = (* si l'on veut que cela preserve le tri il faut que le tri soit lexicographique selon les attributs *)
  (*On récupère la liste des attributs des deux tables*)
  let attributes_list1 = get_table_attributes t1 in
  let attributes_list2 = get_table_attributes t2 in

  (*On crée la liste des attributs de la table résultante*)
  let attributes_listf = attributes_list1 @ attributes_list2 in

  (*On crée la liste des types des attributs de la table résultante*)
  let attributes_type_list = List.(map get_attribute_type attributes_listf) in

  (*On crée la table résultante*)
  let table_prod = create_tmp_table attributes_type_list in
  
  (*On récupère l'itérateur de data de la table 1*)
  let iter_1 = get_table_data t1  in
  let data_1 = ref (iter_1 ()) in

  (*On parcourt les données de la table 1*)
  while !data_1 != None do (

    (*On récupère l'itérateur de data de la table 2*)
    let iter_2 = get_table_data t2 in
    let data_2 = ref (iter_2 ()) in

    (*On parcourt les données de la table 2*)
    while !data_2 != None do (

      (*On insère la ligne correspondante à la concaténation des lignes des tables 1 et 2*)
      (match !data_1, !data_2 with
      | Some x, Some y -> insert_row table_prod (Array.append x y)
      | _ -> ());
      data_2 := iter_2 ());
      (*On récupère la ligne suivante de la table 2*)

    done;
    data_1 := iter_1 ());
    (*On récupère la ligne suivante de la table 1*)
  done;

  (*On retourne la table résultante*)
  table_prod
;;

let find_index (predicate : attribute -> bool) (prev_att : attribute list) : int option =
  let rec aux (prev_att : attribute list) (index : int) =
    match prev_att with
      | [] -> None
      | x :: _ when predicate x -> Some index
      | _ :: tl -> aux tl (index + 1)
  in aux prev_att 0

let proj table att =
  let prev_att = get_table_attributes table in
  let map = Array.make (List.length att) (-1) in
  List.iteri (fun i att -> map.(i) <- (find_index (fun x -> x =@ att) prev_att) |> Option.get) att;
  let attributes_type_list = List.map get_attribute_type att in
  let proj_table = create_tmp_table attributes_type_list in
  let iter = get_table_data table in
  let rec loop = function
    | Some d ->
      let new_data = Array.map (fun i -> d.(i)) map in
      insert_row proj_table new_data;
      loop (iter ())
    | _ -> ()
  in loop (iter ());
  proj_table

(* let proj table attribute_list =  *)
(*   let big_table_attributes_list = get_table_attributes table in *)
(*   let attribute_type_list = List.map get_attribute_type attribute_list in *)
(*   let proj_table = create_tmp_table attribute_type_list in *)
(*   let iter = get_table_data table in *)
(*   let data = ref (iter ()) in *)
(*   while !data != None do (  *)
(*     match !data with *)
(*     | Some d -> *)
(*       let new_data = Array.make (List.length attribute_list) d.(0) in *)
(*       let new_cursor = ref 0 in *)
(*       let old_cursor = ref 0 in *)
(*       let aux x = *)
(*         if !new_cursor < (List.length attribute_list) && x = List.nth attribute_list !new_cursor then *)
(*           ( *)
(*           new_data.(!new_cursor) <- d.(!old_cursor); *)
(*           new_cursor := !new_cursor + 1); *)
(*         old_cursor := !old_cursor + 1 in *)
(*       List.iter aux big_table_attributes_list; *)
(*       insert_row proj_table new_data; *)
(*       new_cursor := 0; *)
(*       old_cursor := 0; *)
(*     | _ -> ()); *)
(*     data := iter (); *)
(*      *)
(*   done; *)
(*   proj_table;; *)

let restriction table ope =
  let table_attributes_list = get_table_attributes table in
  let attribute_type_list = List.map get_attribute_type table_attributes_list in
  let restr_table = create_tmp_table attribute_type_list in
  let iter = get_table_data table in
  let data = ref (iter ()) in
  while !data != None do (
    match !data with
    | Some d ->
      if ope d then
        insert_row restr_table d
    | _ -> ());
    data := iter ();
  done;
  restr_table;;  


(*
let join t1 t2 a1 a2 = 
  let pos1 = get_attribute_position a1 in
  let pos2 = get_attribute_position a2 + List.length (get_table_attributes t1) in  
  let t = prod_cart t1 t2 in (*les attributs *)
  let attributes_list = get_table_attributes t in
  let attributes_type_list = List.map get_attribute_type attributes_list in
  let join_table = create_tmp_table attributes_type_list in
  let iter = get_table_data t in
  let data = ref (iter ()) in
  while !data != None do (
    match !data with
    | Some d -> if d.(pos1) = d.(pos2) then insert_row join_table d
    | _ -> ();
    data := iter ());
  done;
  join_table;;
*)


let compare_attributes_list_type a1 a2 =
  let at1 = List.map get_attribute_type a1 in
  let at2 = List.map get_attribute_type a2 in
  List.iter2 (fun x y -> if x <> y then raise (Invalid_argument "Les attributs n'ont pas le même type")) at1 at2;;


    

let union t1 t2 = 
  let attributes_list1 = get_table_attributes t1 in
  let attributes_list2 = get_table_attributes t2 in
  compare_attributes_list_type attributes_list1 attributes_list2;
  let attribute_type_list = List.map get_attribute_type attributes_list1 in
  let union_table = create_tmp_table attribute_type_list in
  let iter1 = get_table_data t1 in
  let data1 = ref (iter1 ()) in
  while !data1 != None do (
    (match !data1 with
    | Some d -> insert_row union_table d
    | _ -> ());
    data1 := iter1 ());
  done;
  let iter2 = get_table_data t2 in
  let data2 = ref (iter2 ()) in
  while !data2 != None do (
    (match !data2 with
    | Some d -> insert_row union_table d
    | _ -> ());
    data2 := iter2 ());
  done;
  union_table;;


let inter_if_not_sorted t1 t2 = (* algorithme naif *)
  let attributes_list1 = get_table_attributes t1 in
  let attribute_type_list = List.map get_attribute_type attributes_list1 in
  let inter_table = create_tmp_table attribute_type_list in
  let iter1 = get_table_data t1 in
  let data1 = ref (iter1 ()) in
  while !data1 != None do (
    let iter2 = get_table_data t2 in
    let data2 = ref (iter2 ()) in
    while !data2 != None do (
      match !data1, !data2 with
      | Some d1, Some d2 -> if equal_array d1 d2 then insert_row inter_table d1
      | _ -> ();
      data2 := iter2 ());
    done;
    data1 := iter1 ());
  done;
  inter_table;;

let inter_if_sorted t1 t2 iter1 iter2 =
(* algorithme optimisé: les opérateurs iter() nous envoient
les lignes par ordre croissant selon une clé primaire *)
   let attributes_list1 = get_table_attributes t1 in 
   let attributes_list2 = get_table_attributes t2 in 
   let attribute_type_list = List.map get_attribute_type attributes_list1 in 
   let inter_table = create_tmp_table attribute_type_list in 
   let data1 = ref (iter1 ()) in 
   let data2 = ref (iter2 ()) in 
   while !data1 != None do 
     while !data2 != None do 
       match !data1,!data2 with 
         | Some (k1,v1), Some (k2,v2) ->  
           let d1 = get_table_nth_data t1 v1 in let d2 = get_table_nth_data t2 v2 in 
           if k1 =: k2  then (insert_row inter_table d1; data1 := iter1 (); data2 := iter2 ();)
           else (if k2 <: k1 then data2 := iter2 () (* toutes les lignes dans t1 sont plus grandes que d2 *)
           else data1 := iter1 ();)  (* toutes les lignes dans t2 sont plus grandes que d1 *)
         | _ -> (); 
     done 
   done; 
 inter_table;; 

let inter t1 t2 = (* ici on choisit quel algorithme on utilise: soit on a pas de clée primaire et on prend le naif sinon on peut prendre celui optimisé*)
  let attributes_list1 = get_table_attributes t1 in 
  let attributes_list2 = get_table_attributes t2 in 
  compare_attributes_list_type attributes_list1 attributes_list2; 
  let meta1 = table_meta_info t1 in
  let meta2 = table_meta_info t2 in
  match meta1.primary_index,meta2.primary_index with  (* je suppose que vu que les types sont les memes les indexs primaires correspondent egalement ? *)
  |None,None -> inter_if_not_sorted t1 t2
  |Some id_att1, Some id_att2->    
                    let Ind { bptrees = (module Bptree); index = index1 } = get_attribute_index id_att1 |> Option.get in
                    let iter1 = Bptree.get_iterator index1 in
                    let Ind { bptrees = (module Bptree); index = index2 } = get_attribute_index id_att2 |> Option.get in
                    let iter2 = Bptree.get_iterator index2 in
                    inter_if_sorted t1 t2 iter1 iter2
  |_,_ -> failwith "les types sont censés être les mêmes" 




let distinct_if_not_sorted t =
  let attributes_list = get_table_attributes t in
  let attribute_type_list = List.map get_attribute_type attributes_list in
  let distinct_table = create_tmp_table attribute_type_list in
  let iter1 = get_table_data t in
  let element_table1 = ref (iter1 ()) in 
  while !element_table1 != None do
    let b = ref true in 
    let iter2 = get_table_data distinct_table in
    let element_table2 = ref (iter2()) in 
      while !element_table2 !=None do
        match !element_table1, !element_table2 with
        | Some x, Some y -> if (equal_array x y) then b:=false
        | _ -> ();
      done;
      if !b then 
        match !element_table1 with
        | Some x -> insert_row distinct_table x
        | _ -> ();
      done;
  distinct_table
;;

 let distinct_if_sorted t iter = 
   let attributes_list = get_table_attributes t in 
   let attribute_type_list = List.map get_attribute_type attributes_list in 
   let distinct_table = create_tmp_table attribute_type_list in 
   let prev_data = ref (iter ()) in 
   let data = ref (iter ()) in 
   (match !prev_data with 
   |Some (k,v) -> let x = get_table_nth_data t v in insert_row distinct_table x; 
               while !data != None do 
                 match !data with 
                 |Some (k',v') -> let y = get_table_nth_data t v' in  
                 if x = y then data := (iter ()) else (insert_row distinct_table y; prev_data := (!data); data := (iter ());) 
                 | _ -> (); 
               done; 
   | _ -> ()); 
   distinct_table;; 



   let distinct t  =
    let attributes_list = get_table_attributes t in 
    let meta = table_meta_info t in
    match meta.primary_index with  (* je suppose que vu que les types sont les memes les indexs primaires correspondent egalement ? *)
    |None -> distinct_if_not_sorted t
    |Some id_att ->    
                      let Ind { bptrees = (module BPTree); index } = get_attribute_index id_att |> Option.get in
                      let iter = BPTree.get_iterator index in
                      distinct_if_sorted t iter

let minus_if_not_sorted table1 table2 = 
  let attributes_list = get_table_attributes table1 in
  let attribute_type_list = List.map get_attribute_type attributes_list in
  let minus_table = create_tmp_table attribute_type_list in
  let iter1 = get_table_data table1 in
  let element_table1 = ref (iter1()) in 
  while !element_table1 != None do
    let b = ref true in 
    let iter2 = get_table_data table2 in
      let element_table2 = ref (iter2()) in 
      while !element_table2 !=None do
        match !element_table1, !element_table2 with
        | Some x, Some y -> if (equal_array x y) then b:=false
        | _ -> ();
      done;
      if !b then 
        match !element_table1 with
        | Some x -> insert_row minus_table x
        | _ -> ();
      done;
  minus_table
;;


 let minus_if_sorted t1 t2 iter1 iter2 =  
   let attributes_list1 = get_table_attributes t1 in 
   let attributes_list2 = get_table_attributes t2 in 
   let attribute_type_list = List.map get_attribute_type attributes_list1 in 
   let minus_table = create_tmp_table attribute_type_list in  
   let data1 = ref (iter1 ()) in 
   let data2 = ref (iter2 ()) in 
   while !data1 != None do 
     match !data1,!data2 with 
     |Some (k1,v1), None -> let d1 = get_table_nth_data t1 v1 in insert_row minus_table d1 
     |Some (k1,v1), Some (k2,v2)  ->   
       let d1 = get_table_nth_data t1 v1 in 
       let d2 = get_table_nth_data t2 v2 in 
       if (k1 =: k2) then (data1 := iter1 (); data2 := iter2 ();) 
       else( if k2 <: k1 then data2 := iter2 () 
       else (insert_row minus_table d1; data1 := iter1 ();) ) 
     | _ -> (); 
   done; 
   minus_table;; 


let minus t1 t2 =
  let attributes_list1 = get_table_attributes t1 in 
  let attributes_list2 = get_table_attributes t2 in 
  compare_attributes_list_type attributes_list1 attributes_list2; 
  let meta1 = table_meta_info t1 in
  let meta2 = table_meta_info t2 in
  match meta1.primary_index,meta2.primary_index with  (* je suppose que vu que les types sont les memes les indexs primaires correspondent egalement ? *)
  |None,None -> minus_if_not_sorted t1 t2
  |Some id_att1, Some id_att2->    
                    let Ind { bptrees = (module Bptree); index = index1 } = get_attribute_index id_att1 |> Option.get in
                    let iter1 = Bptree.get_iterator index1 in
                    let Ind { bptrees = (module Bptree); index = index2 } = get_attribute_index id_att2 |> Option.get in
                    let iter2 = Bptree.get_iterator index2 in
                    minus_if_sorted t1 t2 iter1 iter2
  |_,_ -> failwith "les types sont censés être les mêmes" 



(*Si vous créez plusieurs algos pour une même opération, merci de les mettre dans ces listes*)
let union_list : (table -> table -> table) list = [union]
let inter_list : (table -> table -> table) list = [inter]
let minus_list : (table -> table -> table) list = [minus]
let prod_list : (table -> table -> table) list = [prod_cart]
let join_list : (table -> table -> attribute list -> attribute list -> table) list = [Sort.block_nested_loop; Sort.sort_merge_join]
let proj_list : (table -> attribute list -> table) list = [proj]
let restriction_list : (table -> (data array -> bool) -> table) list = [restriction]
