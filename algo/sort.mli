open Index
open Data
(*algo de jointure*)
val block_nested_loop : table->table->attribute list->attribute list->table
val sort_merge_join : table->table->attribute list->attribute list->table
val get_relation : attribute list -> attribute list  -> (data array -> data array -> int)