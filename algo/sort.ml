open Data
open Index
let taille_ram:int = 200

let rec taille_attribut (taille_list : int list):int = 
  match taille_list with
    | [] -> 0
    |t::q -> t + (taille_attribut q)
;; 
let rec get_relation (a1:attribute list) (a2:attribute list) : (data array -> data array -> int) = 
  (*renvoie 0 si a1 = a2 , 1  a1 > a2 et -1 sinon*)
  fun ar1 ar2 ->( 
  match (a1, a2) with 
  | [],[] -> 0
  |t::q,a::b ->  let pos1 = get_attribute_position t in
  let pos2 = get_attribute_position a  in
  if ar1.(pos1)  = ar2.(pos2) then (get_relation q b) ar1 ar2
  else (if ar1.(pos1)  < ar2.(pos2) then -1
    else 1)
  | _ -> failwith("not the same length of attribute") )
;;

let rec iter_to_list (it : unit -> data array option) (n:int): data array list = (*consomme un itérateur et le transforme en une liste de taille n*)
  if n=0 then []
  else   
  match it() with
      |None -> []
      |Some(v) -> v::(iter_to_list it (n-1))
;;

let rec fill_table (tab : table) (l) : unit = 
  match l with 
    | [] -> ()
    | t::q -> insert_row tab t ;
      fill_table tab q
;;

let min_table (l: data array option array ) (rel : data array->data array -> int ):int list = (*renvoie la liste de tout les i eme éléments les plus petits*)
  let l_int = ref [] in 
  for i =0 to (Array.length l - 1) do
    match (l.(i),!l_int) with
      |None,_ -> ()
      |Some(_),[] -> l_int := [i];
      |Some(v),t::_ -> let res : int = (rel (Option.get l.(t)) v ) in
        if res = 0 then l_int := i::!l_int 
        else (
          if res = -1 then l_int := [i]
          else ()
        )
  done;
  !l_int
;;


let rec conv (ar1 : 'a array ) (ar2 : 'a array) (n_list : int list(*trié*)) (len_p :int (*longeur du tableau parcouru*)) = (*renvoie la concaténation de ar1 et ar2 sans les i eme termes de ar1*)
    match n_list with
    | [] -> Array.append ar1 ar2
    | t::q -> let len = Array.length ar1 in
      if t-len_p = len - 1 then Array.append (Array.sub ar1 0 (len-1)) ar2
      else (if t = 0 then ( conv (Array.sub ar1 (t+1) (len-1)) ar2 q t)
            else  Array.append (Array.sub ar1 0 t) (conv (Array.sub ar1 (t+1) (len-1)) ar2 q t))

;;
  
let cont_table (table1:table ) (table2:table) (no_att : int list) : table = (*renvoie la table fusion vide sans l'attributs de nom no_att  *)
  let att1 : attribute list = get_table_attributes table1 in 
  let att2 : attribute list = get_table_attributes table2 in  
  let att_merge = conv (Array.of_list (att1)) (Array.of_list (att2)) no_att  0 in
  let new_att = Array.to_list( Array.map (fun att -> get_attribute_type att) att_merge) in
  create_tmp_table new_att
;;

let empty_array ar:bool = 
  let b = ref true  in
  for i = 0 to (Array.length ar - 1) do
    if Option.is_some ar.(i) then b:= false 
  done;
  !b
;;

let use_array (ar: 'a option array) (int_array: int array) (table : table) (l_int : int list) (nb_tuple : int): unit = (* fait en sorte que tout les *)
  List.iter (fun i -> 
  Array.set ar i 
  (int_array.(i)<- int_array.(i) + 1; (*incrémente le nombre d'élément vu*)
  if int_array.(i)>= nb_tuple then None (*regarde si tout les element du bloc ont été vu*)
  else(
  try (Some(get_table_nth_data table (nb_tuple*i + int_array.(i)) )) with 
    |_-> None ))) l_int
;;

let sort_merge_join ( table1 :table) (table2 :table)(a1: attribute list) (a2: attribute list): table = 
  let meta_data1= table_meta_info table1 in
  let meta_data2= table_meta_info table2 in
  let taille_att1 = taille_attribut  meta_data1.attributes_size in(*renvoie la taille d'un tuple d'une table*)
  let taille_att2 = taille_attribut  meta_data2.attributes_size in
  let nb_table1 = (taille_att1*meta_data1.n_tuples / taille_ram)+1 in  (*renvoie le nombre de pages qu'il faut pour la table*)
  let nb_table2 = (taille_att2*meta_data1.n_tuples / taille_ram)+1 in
  let nb_tuple1 =  (taille_ram /taille_att1)+1 in (*renvoie le nombre de tuples dans une page*)
  let nb_tuple2 =  (taille_ram /taille_att2)+1 in

  let iter_1 = get_table_data table1 in
  let iter_2 = get_table_data table2 in

  let rel1 = get_relation a1 a1 in
  let rel2 = get_relation a2 a2 in
  let reltable = get_relation a1 a2 in
  
  let array_table1 : int array = Array.make nb_table1 0 in (*renvoi l'indice de l'élément le plus petit non traité du ième bloc *)
  let array_table2 : int array = Array.make nb_table2 0 in 

  let att_t1: attribute_type list = List.map get_attribute_type (get_table_attributes table1 ) in
  let att_t2: attribute_type list = List.map get_attribute_type (get_table_attributes table2 ) in

  let table_aux1 = create_tmp_table att_t1 in 
  let table_aux2 = create_tmp_table att_t2 in 

  for _ = 0 to (nb_table1 - 1) do
    let l  = iter_to_list iter_1 nb_tuple1 in 
    let l_sort = List.sort rel1 l in 
    fill_table table_aux1 l_sort; 
  done;

  for _ = 0 to (nb_table2 - 1) do
    let l  = iter_to_list iter_2 nb_tuple2 in 
    let l_sort = List.sort rel2 l in 
    fill_table table_aux2 l_sort; 
  done;
  
  let array_min1 = Array.init nb_table1 (fun (i:int):data array option -> Some(get_table_nth_data table_aux1 (nb_tuple1*i))) in (*donne le plus petit élément du i eme bloc*)
  let array_min2 = Array.init nb_table1 (fun (i:int):data array option-> Some(get_table_nth_data table_aux2 (nb_tuple2*i))) in

  let n_list:int list =List.sort (fun i1 i2 -> if i1  = i2 then 0 else (if i1 < i2 then -1 else 1)) (List.map (fun a1 -> get_attribute_position a1) a1) in (*liste trié de tous les index des attributs qu'on va join*)

  let table_join = cont_table table1 table2 n_list in

  while (not (empty_array array_min1 || empty_array array_min2) ) do
    let min1 = min_table array_min1 rel1 in (*donne la liste des bloc contenant les element les plus petit de la table1 *)
    let min2 = min_table array_min1 rel2 in
    let res = reltable (Option.get array_min1.(List.hd min1)) (Option.get array_min2.(List.hd min2) )in 
    if res = -1 then use_array array_min1 array_table1 table_aux1 min1 nb_tuple1
    else if res = 1 then use_array array_min2 array_table2 table_aux1 min2 nb_tuple2
    else (List.iter(
      fun (i:int) :unit -> 
      List.iter (
        fun (j:int) :unit -> 
          let new_arr = (conv (Option.get array_min1.(i)) (Option.get array_min2.(j)) n_list 0) in
          insert_row table_join new_arr;
      ) min2
    ) min1;
    use_array array_min1 array_table1 table_aux1 min1 nb_tuple1;
    use_array array_min2 array_table2 table_aux2 min2 nb_tuple2;)
  done;
  table_join
;;
let rec find_equal_aux (arr : 'a array array) (valu : 'a array) (rel : 'a array -> 'a array -> int) (borne : int) (ident : int(*+1 si il faut regarder en avant et -1 sinon*)) (n : int ) =
  if (n=borne ) then []
  else ( 
  if (rel arr.(n) valu = 0 )then 
  (
    n::(find_equal_aux arr valu rel borne  ident (n+ident))
  )
else [])

let find_equal (arr : 'a array array) (valu : 'a array) (rel : 'a array -> 'a array -> int) (deb : int) (fin : int) (n : int ): int list =
  (if (deb = n)then [] else find_equal_aux arr valu rel deb (-1) (n-1))@(find_equal_aux arr valu rel fin 1 n) (* le if  sert à éviter d'avoir 2 fois le même entier dans la liste*)
 

let rec find_dico (arr : 'a array array) (valu : 'a array) (rel : 'a array -> 'a array -> int) (deb : int) (fin : int): int list = 
  (* renvoie la liste des i eme élément ayant la meme valeur que valu pour rel en prenant en paramètre arr qui est trié*)
  if (deb = fin) then []
  else
  let mid  = (fin + deb)/2 in 
  let res = rel arr.(mid) valu in
    if res = -1 then
    (find_dico arr valu rel (mid+1) fin )
    else (
    if res = 1 then find_dico arr valu rel deb (mid)
    else (
      find_equal arr valu rel deb fin mid
    )
    )
;;


let block_nested_loop (table1 : table) (table2 : table) (at1 : attribute list)  (at2 : attribute list) : table = 
  let meta_data= table_meta_info table1 in
  let taille_att = taille_attribut  meta_data.attributes_size in(*renvoie la taille d'un tuple d'une table*)
  let nb_table = (taille_att*meta_data.n_tuples / taille_ram) +1  in
  let nb_tuple = (taille_ram /taille_att) +1 in
  (* Printf.printf "%d %d" nb_table nb_tuple ; *)

  let iter_s = get_table_data table1 in

  let rel1 = get_relation at1 at1 in
  let reltable = get_relation at1 at2 in
  
  let n_list:int list =List.sort (fun i1 i2 -> if i1  = i2 then 0 else (if i1 < i2 then -1 else 1)) (List.map (fun a1 -> get_attribute_position a1) at1) in (*liste trié de tous les index des attributs qu'on va join*)

  let table_join = cont_table table1 table2 n_list in

  for _ = 0 to (nb_table - 1) do
    let l  = iter_to_list iter_s nb_tuple in 
    (* Printf.printf "%d"( List.length l); *)

    let a_sort = Array.of_list (List.sort rel1 l) in
    let it_new = get_table_data table2  in 
    let op_tuple_big = ref (it_new()) in
    while !op_tuple_big != None do
      let tuple_big = Option.get !op_tuple_big in
      let l_int = find_dico a_sort tuple_big reltable 0 (Array.length a_sort) in
      (* Printf.printf "%d"( List.length l_int); *)
      List.iter (
        fun i -> 
        insert_row table_join (conv a_sort.(i) tuple_big n_list 0 )
        ) l_int;
        op_tuple_big := it_new()
    done;
  done;
  table_join
;;