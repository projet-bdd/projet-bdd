(* Il faut appeler la compil pour avoir la query, puis executer chez optim la query *)


(** Create a test table *)
let create_test_table () =
  (* print_endline "in create_test_table"; *)
  let open Index in
  let att_typs = [create_attribute_type "id" TINTEGER [PRIMARY_KEY]; create_attribute_type "name" (TVARCHAR 16) []; create_attribute_type "size" TREAL []; create_attribute_type "online" TBOOLEAN []; create_attribute_type "note" TINTEGER []] in
  create_table "table" att_typs

let fill_table () =
  (* print_endline "in fill_table"; *)
  let open Index in
  let open Data in
  let tbl = get_table "table" in
  if (Index.table_meta_info tbl).n_tuples = 0 then (
    insert_row tbl [|integer 2l; varchar 16 "hugo"; real 2.1; boolean true; integer 2l|];
    insert_row tbl [|integer 4l; varchar 16 "simon"; real 1.3; boolean false; integer 20l|];
    insert_row tbl [|integer 6l; varchar 16 "sluko"; real 1.6; boolean true; integer 20l|];
    insert_row tbl [|integer 8l; varchar 16 "baptistou"; real 3.2; boolean false; integer 20l|]  
  )

let () =
  (* print_endline "in ()"; *)
  create_test_table ();
  fill_table ();
  (* Index.print_table (Index.get_table "table"); *)
  if Array.length Sys.argv < 2 then failwith "please give a query >:(";
  let sql_query = Sys.argv.(1) in
  let debug_opti = (Array.length Sys.argv > 2  && (Sys.argv.(2) = "-debug-opti")) in
  let query_opt = Compil.compile sql_query in
  match query_opt with
  | None -> failwith "something wrong"
  | Some q -> let result = Compil_query.exec q debug_opti in Index.print_table result
