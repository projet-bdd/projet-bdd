Liste des interfaces proposés par chacuns :

# Indexation

### module index (ou data structure)

- type `table`
- type `attribute`
- type `attribute_type`
- type `constraint`

---

-> __ Les attributs seront toujours recus / donnés dans le même ordre __


- `create_table : string -> attribute_type list -> unit`
- `get_table : string -> table`
- `get_table_data : table -> (unit -> data array option)`
- `get_table_type : table -> attribute array`
- `get_new_name_table : unit -> string`

- `create_attribute_type : string -> data_type -> constraint list -> attribute_type`
- `get_attribute_type_name : attribute_type -> string`
- `get_attribute_type_data_data : attribute_type -> data_type`
- `get_constraint_list : attribute -> constraint list`

- `insert_row : table -> data list -> unit`
- `get_column : table -> string -> column`
- `get_column_name : column -> string`
- `get_column_type : column -> data_type`
- `get_attribute_data : attribute -> data list` ?
- `get_new_name_attribute : unit -> string`
- `get_pos : attribute -> int * table`


### module data

- type `data_type`
- type `data`

---

`(<:) : data -> data -> bool`
`(=:) : data -> data -> bool`
`size : data_type -> int`

et des fonctions de creation de `data_type`
