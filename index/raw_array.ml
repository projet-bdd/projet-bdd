module type RawElt = sig
  type t

  val byte_len : int
  val default : t

  val write : bytes -> int -> t -> unit
  val read : bytes -> int -> t
end

module type S = sig
  type elt
  type t

  val length : t -> int
  val filename : t -> string
  val get : t -> int -> elt
  val set : t -> int -> elt -> unit
  val make : ?filename:(string option) -> int -> t
  val is_temporary : t -> bool
  val append : t -> elt -> unit
end

module Make (Elt : RawElt) = struct
  type elt = Elt.t
  type t = {
    tmp : bool;
    loaded_data : bytes; (* used to write on disk & tmp storage *)
    mutable loaded_page_index : int;
    loaded_page : bytes; (* used to read from disk *)
    mutable len : int;
    filename : string;
    mutable fd : Unix.file_descr option;
  }

  let page_size = 4096
  let max_loaded_size = 100

  let entries_per_page = page_size / Elt.byte_len
  let page_of_index i = i / entries_per_page
  let offset_in_page i = i mod entries_per_page
  let max_loaded_entries = entries_per_page * max_loaded_size

  let file_offset i = page_of_index i * page_size + offset_in_page i * Elt.byte_len
  let post_filled_bytes i =
    if offset_in_page i = entries_per_page - 1 then (page_size - entries_per_page * Elt.byte_len)
    else 0

  let default_filename =
    let counter = ref 0 in
    fun () -> incr counter; "array_" ^ (string_of_int !counter) ^ ".tmp"

  let length { len; _ } = len
  let filename { filename; _ } = filename

  let load_page t ?(force = false) i =
    if force || i <> t.loaded_page_index then (
      let fd = Option.get t.fd in
      Unix.lseek fd (i * page_size) Unix.SEEK_SET |> ignore;
      Unix.read fd t.loaded_page 0 page_size |> ignore;
      t.loaded_page_index <- i
    )

  let get t i =
    if i < 0 || i >= t.len then raise (Invalid_argument "Array.get")
    else
        if Option.is_some t.fd then (
          let page = page_of_index i in
          load_page t page;
          Elt.read t.loaded_page ((offset_in_page i) * Elt.byte_len)
          (* let fd = Option.get t.fd and offset = file_offset i in *)
          (* Unix.lseek fd offset Unix.SEEK_SET |> ignore; *)
          (* Unix.read fd t.loaded_data 0 Elt.byte_len |> ignore; *)
          (* Elt.read t.loaded_data 0 *)
        ) else Elt.read t.loaded_data (file_offset i)


  let set t i v =
    if i < 0 || i >= t.len then raise (Invalid_argument "Array.set")
    else
      if Option.is_some t.fd then (
        let fd = Option.get t.fd and offset = file_offset i in
        Elt.write t.loaded_data 0 v;
        Unix.lseek fd offset Unix.SEEK_SET |> ignore;
        Unix.write fd t.loaded_data 0 Elt.byte_len |> ignore;
        if page_of_index i = t.loaded_page_index then
          Elt.write t.loaded_page ((offset_in_page i) * Elt.byte_len) v
      ) else Elt.write t.loaded_data (file_offset i) v

  let make ?(filename = None) n =
    let buf = Bytes.create (max_loaded_size * page_size) in
    let page_buf = Bytes.create page_size in
    let tmp = Option.is_none filename in
    let fname = match filename with
      | None -> default_filename ()
      | Some f -> f
    in
    let n_fresh = Sys.file_exists fname in
    let fd_opts = if n_fresh then [Unix.O_RDWR] else [Unix.O_RDWR; Unix.O_CREAT] in
    let fd =
      if Option.is_some filename || n > max_loaded_entries then Some (Unix.openfile fname fd_opts 0o600)
      else None
    in
    let arr = { tmp; loaded_data = buf; loaded_page_index = -1; loaded_page = page_buf; len = n; filename = fname ; fd } in
    if Option.is_some fd && n_fresh then
      let st = Unix.fstat (Option.get fd) in
      let len = st.st_size / Elt.byte_len in
      arr.len <- len
    else
      for i = 0 to n - 1 do
        set arr i Elt.default
      done;
    arr

  let is_temporary t = t.tmp

  let append t v =
    if Option.is_some t.fd || (t.len + 1) >= max_loaded_entries then (
      let fd =
        if Option.is_none t.fd then
          let fd = Unix.openfile t.filename [Unix.O_RDWR; Unix.O_CREAT] 0o600 in
          t.fd <- Some fd;
          Unix.write fd t.loaded_data 0 (page_size * max_loaded_size) |> ignore;
          fd
        else Option.get t.fd
      in
      let len = Elt.byte_len + post_filled_bytes t.len in
      Elt.write t.loaded_data 0 v;
      Bytes.fill t.loaded_data Elt.byte_len (post_filled_bytes t.len) '\x00';
      Unix.lseek fd 0 Unix.SEEK_END |> ignore;
      Unix.write fd t.loaded_data 0 len |> ignore;
      if page_of_index t.len = t.loaded_page_index then load_page t ~force:true t.loaded_page_index;
      t.len <- t.len + 1
    ) else (
      Elt.write t.loaded_data (file_offset t.len) v;
      (* Bytes.fill t.loaded_data ((t.len + 1) * Elt.byte_len) (post_filled_bytes t.len) '\x00'; *)
      t.len <- t.len + 1
    )

end
