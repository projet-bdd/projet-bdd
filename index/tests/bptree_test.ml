module Opt = struct
  type key = int
  type value = int 
  let order = 4
  let compare = Int.compare
end

module RawInt = struct
  
  type t = int
  let byte_len = 4
  let default = 0
  let write by pos i = Bytes.set_int32_le by pos (Int32.of_int i)
  let read by pos = Bytes.get_int32_le by pos |> Int32.to_int

end

module BPTree = Bptrees.Make (Opt) (RawInt) (RawInt)

let bptree () =
  let open BPTree in
  Printf.printf "Running BPTree tests :\n";
  let t = 
    if Sys.file_exists "test.bptree" then create "test.bptree"
    else (let t = create "test.bptree" in
    insert t 5 4;
    insert t 3 2;
    insert t 10 9;
    insert t 1 0;
    insert t 4 3;
    insert t 7 6;
    insert t 9 8;
    insert t 2 1;
    insert t 6 5;
    insert t 12 11;
    insert t 11 10;
    insert t 13 12;
    insert t 14 13;
    insert t 16 15;
    insert t 15 14;
    insert t 17 16; t)
  in
  (* show t |> print_endline; *)
  let it = get_iterator t in
  let rec loop () =
    match it () with
    | None -> ()
    | Some (k, v) -> Printf.printf "%d -> %d\n" k v; loop ()
  in
  loop ()
