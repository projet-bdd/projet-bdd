module RawInt = struct
  
  type t = int
  let byte_len = 4
  let default = 0
  let write by pos i = Bytes.set_int32_le by pos (Int32.of_int i)
  let read by pos = Bytes.get_int32_le by pos |> Int32.to_int

end

module RArray = Raw_array.Make (RawInt)

let raw_array () =
  Printf.printf "Running Raw_array tests:\n";
  let arr = 
    if Sys.file_exists "test.rarray" then RArray.make ~filename:(Some "test.rarray") 100
    else let t = RArray.make ~filename:(Some "test.rarray") 100 in
      for i = 0 to 99 do
        RArray.set t i (2*i)
      done;
      t
  in
  for i = 0 to 99 do
    Printf.printf " %d-th value = %d\n" i (RArray.get arr i)
  done;
  Printf.printf "Testing a tmp array larger than 100 pagedisk :\n";
  let arr_tmp = RArray.make 102300 in
  RArray.set arr_tmp 102200 1000000;
  for _ = 1 to 200 do RArray.append arr_tmp 42 done;
  Printf.printf " 102200-th value = %d\n" (RArray.get arr_tmp 102200);
  Printf.printf " 102300-th value = %d\n" (RArray.get arr_tmp 102300);
  Printf.printf " 102450-th value = %d\n" (RArray.get arr_tmp 102450)
