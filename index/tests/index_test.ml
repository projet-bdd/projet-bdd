open Index
open Data

let create_table () =
  let att = [create_attribute_type "id" TINTEGER [PRIMARY_KEY]; create_attribute_type "username" (TVARCHAR 16) []; create_attribute_type "score" TREAL []] in
  create_table "users" att

let fill_table () =
  let tbl = get_table "users" in
  for i = 44 to 148 do
    insert_row tbl [|integer (Int32.of_int i); varchar 16 "jeannot"; real 10.0|]
  done;
  insert_row tbl [|integer 1l; varchar 16 "toto"; real 12.5|];
  insert_row tbl [|integer 2l; varchar 16 "titi"; real 15.0|];
  insert_row tbl [|integer 3l; varchar 16 "tutu"; real 10.0|];
  insert_row tbl [|integer 4l; varchar 16 "tata"; real 20.0|];
  insert_row tbl [|integer 5l; varchar 16 "tete"; real 17.5|];
  insert_row tbl [|integer 6l; varchar 16 "jean"; real 15.0|];
  insert_row tbl [|integer 7l; varchar 16 "paul"; real 10.0|];
  insert_row tbl [|integer 8l; varchar 16 "jacques"; real 20.0|];
  insert_row tbl [|integer 9l; varchar 16 "pierre"; real 17.5|];
  insert_row tbl [|integer 10l; varchar 16 "henri"; real 15.0|];
  insert_row tbl [|integer 11l; varchar 16 "michel"; real 10.0|];
  insert_row tbl [|integer 12l; varchar 16 "robert"; real 20.0|];
  insert_row tbl [|integer 13l; varchar 16 "bernard"; real 17.5|];
  insert_row tbl [|integer 14l; varchar 16 "francois"; real 15.0|];
  insert_row tbl [|integer 15l; varchar 16 "alain"; real 10.0|];
  insert_row tbl [|integer 16l; varchar 16 "olivier"; real 20.0|];
  insert_row tbl [|integer 17l; varchar 16 "thomas"; real 17.5|];
  insert_row tbl [|integer 18l; varchar 16 "nicolas"; real 15.0|];
  insert_row tbl [|integer 19l; varchar 16 "jerome"; real 10.0|];
  insert_row tbl [|integer 20l; varchar 16 "raphael"; real 20.0|];
  insert_row tbl [|integer 21l; varchar 16 "gabriel"; real 17.5|];
  insert_row tbl [|integer 22l; varchar 16 "emmanuel"; real 15.0|];
  insert_row tbl [|integer 23l; varchar 16 "jean-michel"; real 10.0|];
  insert_row tbl [|integer 24l; varchar 16 "jean-pierre"; real 20.0|];
  insert_row tbl [|integer 25l; varchar 16 "jean-henri"; real 17.5|];
  insert_row tbl [|integer 26l; varchar 16 "jean-marie"; real 15.0|];
  insert_row tbl [|integer 27l; varchar 16 "jean-louis"; real 10.0|];
  insert_row tbl [|integer 28l; varchar 16 "jean-jacques"; real 20.0|];
  insert_row tbl [|integer 29l; varchar 16 "jean-paul"; real 17.5|];
  insert_row tbl [|integer 30l; varchar 16 "jean-claude"; real 15.0|];
  insert_row tbl [|integer 31l; varchar 16 "jean-philippe"; real 10.0|];
  insert_row tbl [|integer 32l; varchar 16 "jean-francois"; real 20.0|];
  insert_row tbl [|integer 33l; varchar 16 "jean-bernard"; real 17.5|];
  insert_row tbl [|integer 34l; varchar 16 "jean-robert"; real 15.0|];
  insert_row tbl [|integer 36l; varchar 16 "jean-olivier"; real 20.0|];
  insert_row tbl [|integer 37l; varchar 16 "jean-thomas"; real 17.5|];
  insert_row tbl [|integer 38l; varchar 16 "jean-nicolas"; real 15.0|];
  insert_row tbl [|integer 39l; varchar 16 "jean-jerome"; real 10.0|];
  insert_row tbl [|integer 40l; varchar 16 "jean-raphael"; real 20.0|];
  insert_row tbl [|integer 41l; varchar 16 "jean-gabriel"; real 17.5|];
  insert_row tbl [|integer 42l; varchar 16 "jean-emmanuel"; real 15.0|];
  insert_row tbl [|integer 43l; varchar 16 "jean-jean"; real 10.0|]

let index () =
  let tbl = get_table "users" in
  let meta_inf = table_meta_info tbl in
  Printf.printf "Table name : %s\n" (get_table_name tbl);
  Printf.printf "Table 24-th name : %s\n" ((get_table_nth_data tbl 24).(1) |> string_of_data);
  Printf.printf "Table meta info : { n_tuples = %d; n_attributes = %d; primary_index = %s }\n" meta_inf.n_tuples meta_inf.n_attributes (meta_inf.primary_index |> Option.get |> get_attribute_type |> get_attribute_type_name);
  Printf.printf "Table attributes sizes:\n";
  List.iter (fun x -> Printf.printf "- %d\n" x) (meta_inf.attributes_size);
  Printf.printf "Printing table:\n";
  print_table tbl;
  Printf.printf "Using the index iterator :\n";
  let id_att = get_attribute tbl "id" in
  let Ind { bptrees = (module BPTree); index } = get_attribute_index id_att |> Option.get in
  let it = BPTree.get_iterator index in
  let rec loop = function
    | Some (k, v) -> Printf.printf "Key: %s, Name in the tuple: %s\n" (string_of_data k) ((get_table_nth_data tbl v).(1) |> string_of_data); loop (it ())
    | None -> ()
  in loop (it ())

let () =
  if not (Sys.file_exists "users") then (create_table (); fill_table ());
  Raw_array_test.raw_array ();
  Bptree_test.bptree ();
  Printf.printf "Running Index tests:\n";
  index ()
