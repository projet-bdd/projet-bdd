open Data

module type FileTable = sig

  type t (* Table sur le disk *)

  val create : string -> data_type list -> bool -> t

  val filename : t -> string
  val temporary : t -> bool

  val length : t -> int

  val get_iterator : t -> unit -> data array option
  val get : t -> int -> data array

  val append_data : t -> data array -> unit

end


module CSVTable = struct

  type t = { filename : string; typs : data_type list ; temporary : bool}

  let filename table = table.filename
  let temporary table = table.temporary

  let create filename typs temporary = { filename; typs; temporary }

  let length t =
    let stream = open_in t.filename in
    let rec aux acc =
      try
        ignore (input_line stream);
        aux (acc + 1)
      with
        | End_of_file -> close_in stream; acc
    in
    aux 0

  let get_iterator t =
    let stream = open_in t.filename in
    fun () -> (
      try
        let line = input_line stream in
        let datas = List.map2 data_of_string t.typs (String.split_on_char ',' line) in (* warning : if varchar contains ',' it will be splitted *)
        Some (Array.of_list datas)
      with
        | End_of_file -> close_in stream; None
        | Sys_error _ -> None
    )

  let get _ _ = failwith "get not implemented for CSVTable"
  
  let append_data t datas =
    let stream = open_out_gen [Open_wronly; Open_append; Open_creat] 0o666 t.filename in
    let line = String.concat "," (List.map string_of_data (Array.to_list datas)) in
    output_string stream (line ^ "\n"); close_out stream

end

module RawTable = struct

  type t = Arr : { raw_array : (module Raw_array.S with type elt = data array and type t = 'a) ; arr : 'a } -> t

  let create fname typs tmp =
    let (module RArray) = Data.make_data_raw_array typs in
    let filename = if tmp then None else Some fname in
    let arr = RArray.make ~filename:filename 0 in
    Arr { raw_array = (module RArray) ; arr }

  let filename (Arr { raw_array = (module RArray) ; arr }) = RArray.filename arr
  let temporary (Arr { raw_array = (module RArray) ; arr }) = RArray.is_temporary arr

  let length (Arr { raw_array = (module RArray) ; arr }) = RArray.length arr

  let get_iterator (Arr { raw_array = (module RArray) ; arr }) =
    let i = ref 0 in
    fun () -> (
      if !i < RArray.length arr then (
        let res = RArray.get arr !i in
          incr i;
          Some res
      ) else None
    )

  let get (Arr { raw_array = (module RArray) ; arr }) i = RArray.get arr i

  let append_data (Arr { raw_array = (module RArray) ; arr }) datas = RArray.append arr datas

end
