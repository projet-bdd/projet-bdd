(** This module presents an interface to correctly manipulate tables in the database. *)

open Data

(** Types abstracting the database structure. *)

type index = Ind : { bptrees : (module Bptrees.S with type t = 'a and type key = data and type value = int); index : 'a} -> index
type table
type attribute
type attribute_type
type constr =
  | PRIMARY_KEY
  | FOREIGN_KEY
  | UNIQUE

(** [type meta_info] is a record containing metadata about a table. The type is clear since other parts needs to be able to use its definition *)
type meta_info = {n_tuples : int; n_attributes : int; attributes_size : int list; primary_index : attribute option}

(** An equality for attributes. (The polymorphic equality may raise an exception). *)
val (=@) : attribute -> attribute -> bool

(** The following functions are the ones that should be used to manipulate tables. *)

val create_table : string -> attribute_type list -> unit
val create_tmp_table : attribute_type list -> table
val get_table : string -> table
val get_table_name : table -> string
val get_table_data : table -> unit -> data array option
val get_table_nth_data : table -> int -> data array
val get_table_attributes : table -> attribute list
val insert_row : table -> data array -> unit
val table_meta_info : table -> meta_info


(** The following functions are the ones that should be used to manipulate attribute types. *)

val create_attribute_type : string -> data_type -> constr list -> attribute_type
val get_attribute_type_name : attribute_type -> string
val get_attribute_type_data_type : attribute_type -> data_type
val get_attribute_type_constraints : attribute_type -> constr list
val update_attribute_type_display_name : attribute_type -> string -> unit


(** The following functions are the ones that should be used to manipulate attributes. *)

val get_attribute : table -> string -> attribute
val get_attribute_type : attribute -> attribute_type
val get_attribute_position : attribute -> int
val get_attribute_index : attribute -> index option


(** Printing functions *)

(** [print_table t] prints the table [t] to the standard output using simple ascii art. *)
val print_table : table -> unit
