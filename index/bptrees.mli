(** The signature of the tree options module *)
module type TREE_OPTIONS = sig
  
  (** The type of the key *)
  type key

  (** The type of the value *)
  type value

  (** The order of the tree *)
  val order : int
  
  (** The type key needs to be comparable *)
  val compare : key -> key -> int

end

(** The signature of the tree module *)
module type S = sig
  
  (** The type of the key *)
  type key

  (** The type of the value *)
  type value

  (** The type of the tree *)
  type t

  (** [create filenname] creates a new tree with the given filename *)
  val create : string -> t

  (** [find t k] returns the value associated with the key [k] in the tree [t] *)
  val find : t -> key -> value option

  (** [insert t k v] inserts the key [k] with the value [v] in the tree [t] *)
  val insert : t -> key -> value -> unit

  (** [get_iterator t] returns an iterator on the tree [t] where keys are ordered *)
  val get_iterator : t -> unit -> (key * value) option

  (** [get_iterator_from_key t k] returns an iterator on the tree [t] where keys are ordered and the iterator starts from the key [k] *)
  val get_iterator_from_key : t -> key -> unit -> (key * value) option

end


(** The functor to create a tree module *)
module Make (Options : TREE_OPTIONS) (_ : Raw_array.RawElt with type t = Options.key) (_ : Raw_array.RawElt with type t = Options.value) : S with type key = Options.key and type value = Options.value
