module type TREE_OPTIONS = sig
  
  type key
  type value

  val order : int
  
  val compare : key -> key -> int

end

module RawNode (Options : TREE_OPTIONS) (RawKey : Raw_array.RawElt with type t = Options.key) (RawValue : Raw_array.RawElt with type t = Options.value) = struct
   
  type leaf = { values : (Options.key * Options.value) option array; next : int } (* need to be immutable : will be store on the disk. We will avoid side effect with the array *)
  type internal = { dir : (Options.key * int) option array; default_dir : int } (* need to be immutable : will be store on the disk. We will avoid side effect with the array *)
  type t =
    | Leaf of leaf
    | Internal of internal

  let byte_len = 1 + 4 + Options.order * (1 + RawKey.byte_len + (max 4 RawValue.byte_len))
  let default = Leaf { values = Array.make Options.order None; next = -1 }

  let write by pos = function
    | Leaf l ->
      Bytes.set by pos ('\x00');
      Bytes.set_int32_le by (pos + 1) (Int32.of_int l.next);
      Array.iteri (fun i opt ->
        let cur_pos = pos + 5 + i * (1 + RawKey.byte_len + RawValue.byte_len) in
        match opt with
        | None -> Bytes.set by cur_pos '\x00'
        | Some (k, v) ->
          Bytes.set by cur_pos '\x01';
          RawKey.write by (cur_pos + 1) k;
          RawValue.write by (cur_pos + 1 + RawKey.byte_len) v
      ) l.values
    | Internal d ->
      Bytes.set by pos ('\x01');
      Bytes.set_int32_le by (pos + 1) (Int32.of_int d.default_dir);
      Array.iteri (fun i opt ->
        let cur_pos = pos + 5 + i * (1 + RawKey.byte_len + 4) in
        match opt with
        | None -> Bytes.set by cur_pos '\x00'
        | Some (k, p) ->
          Bytes.set by cur_pos '\x01';
          RawKey.write by (cur_pos + 1) k;
          Bytes.set_int32_le by (cur_pos + 1 + RawKey.byte_len) (Int32.of_int p)
      ) d.dir

  let read by pos = match Bytes.get by pos with
    | '\x00' ->
      let next = Bytes.get_int32_le by (pos + 1) |> Int32.to_int in
      let values = Array.init Options.order (fun i ->
        let cur_pos = pos + 5 + i * (1 + RawKey.byte_len + RawValue.byte_len) in
        match Bytes.get by cur_pos with
        | '\x00' -> None
        | '\x01' ->
          let k = RawKey.read by (cur_pos + 1) in
          let v = RawValue.read by (cur_pos + 1 + RawKey.byte_len) in
          Some (k, v)
        | _ -> raise (Invalid_argument "read")
      ) in
      Leaf { values; next }
    | '\x01' ->
      let default_dir = Bytes.get_int32_le by (pos + 1) |> Int32.to_int in
      let dir = Array.init Options.order (fun i ->
        let cur_pos = pos + 5 + i * (1 + RawKey.byte_len + 4) in
        match Bytes.get by cur_pos with
        | '\x00' -> None
        | '\x01' ->
          let k = RawKey.read by (cur_pos + 1) in
          let p = Bytes.get_int32_le by (cur_pos + 1 + RawKey.byte_len) |> Int32.to_int in
          Some (k, p)
        | _ -> raise (Invalid_argument "read")
      ) in
      Internal { dir; default_dir }
    | _ -> raise (Invalid_argument "read")

end

module type S = sig
  
  type key
  type value
  type t

  val create : string -> t
  val find : t -> key -> value option
  val insert : t -> key -> value -> unit
  val get_iterator : t -> unit -> (key * value) option
  val get_iterator_from_key : t -> key -> unit -> (key * value) option

end

module Make (Options : TREE_OPTIONS) (RawKey : Raw_array.RawElt with type t = Options.key) (RawValue : Raw_array.RawElt with type t = Options.value) = struct
  
  module RNode = RawNode (Options) (RawKey) (RawValue)
  module RArray = Raw_array.Make (RNode)

  type key = Options.key
  type value = Options.value
  type t = { nodes : RArray.t; mutable root : int }

  let create name = { nodes = RArray.make ~filename:(Some name) 1 ; root = 0 }

  let get_leaf t i =
    match RArray.get t.nodes i with
    | Leaf l -> l
    | _ -> failwith "not a leaf"

  let get_internal t i =
    match RArray.get t.nodes i with
    | Internal d -> d
    | _ -> failwith "not an internal node"

  let find_leaf t k =
    let q = Queue.create () in
    let rec aux i =
      match RArray.get t.nodes i with
      | Leaf _ -> i
      | Internal d ->
        let rec loop i =
          if i >= Options.order then aux d.default_dir
          else
            match d.dir.(i) with
            | None -> aux d.default_dir
            | Some (k', i') ->
              if Options.compare k k' < 0 then
                aux i'
              else
                loop (i + 1)
        in
        Queue.push i q;
        loop 0
    in aux t.root, q

  let find t k =
    let l_i, _ = find_leaf t k in
    let l = get_leaf t l_i in
    let rec loop i =
      if i >= Options.order then None
      else
        match l.values.(i) with
        | None -> None
        | Some (k', v) ->
          if Options.compare k k' = 0 then Some v
          else loop (i + 1)
    in
    loop 0

  (* -- insert -- *)
  let is_full opt_a =
    let len = Array.length opt_a in
    len = 0 || opt_a.(len - 1) <> None

  (* No side effect *)
  let split_array a =
    let len = Array.length a in
    let mid = len / 2 in
    let a1 = Array.make Options.order None and a2 = Array.make Options.order None in
    for i = 0 to mid - 1 do
      a1.(i) <- a.(i)
    done;
    for i = mid to len - 1 do
      a2.(i - mid) <- a.(i)
    done;
    a1, a2

  (* Side effect on a *)
  let insert_in_values a k v =
    let len = Array.length a in
    let rec loop i k v =
      if i >= len then raise (Invalid_argument "insert_in_values")
      else
        match a.(i) with
        | None -> a.(i) <- Some (k, v)
        | Some (k', v') ->
          if Options.compare k k' < 0 then (
            a.(i) <- Some (k, v);
            loop (i + 1) k' v'
          )
          else if Options.compare k k' = 0 then a.(i) <- Some (k, v)
          else loop (i + 1) k v
    in
    loop 0 k v

  let insert_in_leaf t k v l_i =
    let l = get_leaf t l_i in
    if is_full l.values then (
      let a1, a2 = split_array l.values in
      let mid_k, _ = Option.get a2.(0) in
      if Options.compare k mid_k < 0 then insert_in_values a1 k v else insert_in_values a2 k v;
      let new_l_i = RArray.length t.nodes in
      let up_n = RNode.Leaf { values = a1; next = new_l_i } in
      let new_n = RNode.Leaf { values = a2; next = l.next } in
      RArray.set t.nodes l_i up_n;
      RArray.append t.nodes new_n;
      Some (mid_k, new_l_i)
    ) else (insert_in_values l.values k v; RArray.set t.nodes l_i (RNode.Leaf l); None)


  let split_mid a =
    let len = Array.length a in
    let a1 = Array.make Options.order None and a2 = Array.make Options.order None in
    let mid = len / 2 in
    for i = 0 to mid - 1 do
      a1.(i) <- a.(i)
    done;
    for i = mid + 1 to len - 1 do
      a2.(i - mid) <- a.(i)
    done;
    a1, a.(mid), a2

  (* Side effect on a *)
  let insert_in_dir a k p default_dir =
    let len = Array.length a in
    let rec loop i =
      if i >= len then raise (Invalid_argument "insert_in_dir")
      else
        match a.(i) with
        | None -> a.(i) <- Some (k, default_dir); p
        | Some (k', v') ->
          if Options.compare k k' < 0 then (
            a.(i) <- Some (k, v');
            insert_in_values a k' p;
            default_dir
          )
          else if Options.compare k k' = 0 then failwith "behavior undefined"
          else loop (i + 1)
    in
    loop 0
          

  let insert_in_internal t k p i_i =
    let i = get_internal t i_i in
    if is_full i.dir then (
      let new_a = Array.make (Options.order + 1) None in
      for j = 0 to Options.order - 1 do
        new_a.(j) <- i.dir.(j)
      done;
      let new_ddir = insert_in_dir new_a k p i.default_dir in
      let a1, mid_opt, a2 = split_mid new_a in
      let mid_k, old_mid_p = Option.get mid_opt in
      let mid_p = RArray.length t.nodes in
      let up_n = RNode.Internal { dir = a1; default_dir = old_mid_p } in
      let new_n = RNode.Internal { dir = a2; default_dir = new_ddir } in
      RArray.set t.nodes i_i up_n;
      RArray.append t.nodes new_n;
      Some (mid_k, mid_p)
    ) else (
      let new_default_dir = insert_in_dir i.dir k p i.default_dir in
      RArray.set t.nodes i_i (RNode.Internal { dir = i.dir; default_dir = new_default_dir });
      None
    )


  let insert t k v =
    let l_i, q = find_leaf t k in
    match insert_in_leaf t k v l_i with
    | None -> ()
    | Some (mid_k, new_l_i) ->
      let rec loop k p =
        if Queue.is_empty q then (
          let new_dir = Array.make Options.order None in
          let new_root = RArray.length t.nodes in
          new_dir.(0) <- Some (k, t.root);
          let new_root_n = RNode.Internal { dir = new_dir; default_dir = p } in
          RArray.append t.nodes new_root_n;
          t.root <- new_root
        ) else
          let i_i = Queue.pop q in
          match insert_in_internal t k p i_i with
          | None -> ()
          | Some (mid_k, i_i) -> loop mid_k i_i
      in
      loop mid_k new_l_i

  let get_iterator_from t start_l_i start_i =
    let cur_l = ref (get_leaf t start_l_i) and i = ref start_i in
    fun () ->
      if (!i >= Options.order || !cur_l.values.(!i) = None) && !cur_l.next = -1 then None
      else (
        if !i >= Options.order  || !cur_l.values.(!i) = None then (i := 0; cur_l := get_leaf t (!cur_l).next);
        let v = !cur_l.values.(!i) in
        incr i;
        v
      )


  let get_iterator t = get_iterator_from t 0 0

  let get_iterator_from_key t k =
    let start_l_i = find_leaf t k |> fst in
    let start_i =
      let l = get_leaf t start_l_i in
      let rec loop i =
        if i >= Options.order then failwith "unreachable"
        else
          match l.values.(i) with
          | None -> loop (i + 1)
          | Some (k', _) ->
            if Options.compare k k' = 0 then i
            else loop (i + 1)
      in
      loop 0
    in
    get_iterator_from t start_l_i start_i



end


(* -- Test -- *)
(* module Opt = struct *)
(*   type key = int *)
(*   type value = int  *)
(*   let order = 4 *)
(*   let compare = Int.compare *)
(* end *)
(**)
(* module RawInt = struct *)
(*    *)
(*   type t = int *)
(*   let byte_len = 4 *)
(*   let default = 0 *)
(*   let write by pos i = Printf.printf "writing at pos %d\n" pos; Bytes.set_int32_le by pos (Int32.of_int i) *)
(*   let read by pos = Bytes.get_int32_le by pos |> Int32.to_int *)
(**)
(* end *)
(**)
(* module BPTree = Make (Opt) (RawInt) (RawInt) *)
(**)
(* let () = *)
(*   let open BPTree in *)
(*   let t =  *)
(*     if Sys.file_exists "test.bptree" then create "test.bptree" *)
(*     else (let t = create "test.bptree" in *)
(*   (* insert t 5 4; *) *)
(*   (* insert t 3 2; *) *)
(*   (* insert t 10 9; *) *)
(*   (* insert t 1 0; *) *)
(*   (* insert t 4 3; *) *)
(*   (* insert t 7 6; *) *)
(*   (* insert t 9 8; *) *)
(*   (* insert t 2 1; *) *)
(*   (* insert t 6 5; *) *)
(*   (* insert t 12 11; *) *)
(*   (* insert t 11 10; *) *)
(*   (* insert t 13 12; *) *)
(*   (* insert t 14 13; *) *)
(*   (* insert t 16 15; *) *)
(*   insert t 15 14; *)
(*       insert t 17 16; t) *)
(*   in *)
(*   (* show t |> print_endline; *) *)
(*   let it = get_iterator t in *)
(*   let rec loop () = *)
(*     match it () with *)
(*     | None -> () *)
(*     | Some (k, v) -> Printf.printf "%d -> %d\n" k v; loop () *)
(*   in *)
(*   loop () *)
