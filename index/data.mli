(** This module presents an interface to correctly manipulate data in the database. *)

(** This type defines the data types that can be used in the database. *)
type data_type =
  | TVARCHAR of int (* = string *)
  | TINTEGER (* = int32 *)
  | TREAL (* = float *)
  | TBOOLEAN (* = bool *)

(** This type defines the data that is stored in the database. *)
type data

(** This exception is raised when a data type is invalid. *)
exception Invalid_type

(** This exception is raised when a comparison between two data is invalid. *)
exception Invalid_comparaison of data * data

(* Data typs *)

val data_typ_of_string : string -> data_type
val data_typ_to_string : data_type -> string

(* Data *)

val (=:) : data -> data -> bool
val (<:) : data -> data -> bool

(** The following functions allow to create data of a given type. *)

val varchar : int -> string -> data
val integer : int32 -> data
val real : float -> data
val boolean : bool -> data
val data_of_string : data_type -> string -> data
val string_of_data : data -> string


(** The following functions allow to retrieve the value of a data. *)

val get_string : data -> string
val get_int : data -> int32
val get_float : data -> float
val get_bool : data -> bool



(** The end of the module are functions that bridges data and bytes *)

(** [data_type_size typ] returns the byte length of a data_type. *)
val data_type_size : data_type -> int

(** [make_data_raw_array typs] returns a module that allows to manipulate raw arrays of data. *)
val make_data_raw_array : data_type list -> (module Raw_array.S with type elt = data array)

(** [get_bptree_module typ] return a B+ tree module for the given data type. *)
val get_bptree_module : data_type -> (module Bptrees.S with type key = data and type value = int)
