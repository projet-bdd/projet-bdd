type data_type =
  | TVARCHAR of int (* = string *)
  | TINTEGER (* = int32 *)
  | TREAL (* = float *)
  | TBOOLEAN (* = bool *)

type data =
  | VARCHAR of int * string
  | INTEGER of int32
  | REAL of float
  | BOOLEAN of bool

exception Invalid_type
exception Invalid_comparaison of data * data

(* Data typs *)

let data_typ_of_string s =
  match String.split_on_char ':' s with
  | ["VARCHAR"; i] -> TVARCHAR (int_of_string i)
  | ["INTEGER"] -> TINTEGER
  | ["REAL"] -> TREAL
  | ["BOOLEAN"] -> TBOOLEAN
  | _ -> raise Invalid_type

let data_typ_to_string = function
  | TVARCHAR i -> "VARCHAR:" ^ string_of_int i
  | TINTEGER -> "INTEGER"
  | TREAL -> "REAL"
  | TBOOLEAN -> "BOOLEAN"

(* Data *)

let (<:) d1 d2 =
  match d1, d2 with
  | VARCHAR (_,s1), VARCHAR (_,s2) -> String.compare s1 s2 < 0
  | INTEGER i1, INTEGER i2 -> Int32.compare i1 i2 < 0
  | REAL f1, REAL f2 -> Float.compare f1 f2 < 0
  | BOOLEAN b1, BOOLEAN b2 -> Bool.compare b1 b2 < 0
  | _ -> raise (Invalid_comparaison (d1, d2))

let (=:) d1 d2 =
  match d1, d2 with
  | VARCHAR (_,s1), VARCHAR (_,s2) -> String.compare s1 s2 = 0
  | INTEGER i1, INTEGER i2 -> Int32.compare i1 i2 = 0
  | REAL f1, REAL f2 -> Float.compare f1 f2 = 0
  | BOOLEAN b1, BOOLEAN b2 -> Bool.compare b1 b2 = 0
  | _ -> raise (Invalid_comparaison (d1, d2))


let varchar i s = VARCHAR (i, s)
let integer i = INTEGER i
let real f = REAL f
let boolean b = BOOLEAN b
let data_of_string typ s =
  match typ with
  | TVARCHAR i -> varchar i s
  | TINTEGER -> Int32.of_string s |> integer
  | TREAL -> float_of_string s |> real
  | TBOOLEAN -> bool_of_string s |> boolean
let string_of_data = function
  | VARCHAR (i,s) -> if i < String.length s then String.sub s 0 i else s
  | INTEGER i -> Int32.to_string i
  | REAL f -> string_of_float f
  | BOOLEAN b -> string_of_bool b

let get_string = function
  | VARCHAR (_,s) -> s
  | _ -> raise Invalid_type

let get_int = function
  | INTEGER i -> i
  | _ -> raise Invalid_type

let get_float = function
  | REAL f -> f
  | _ -> raise Invalid_type

let get_bool = function
  | BOOLEAN b -> b
  | _ -> raise Invalid_type


(* Raw data *)

let data_type_size = function
  | TVARCHAR i -> i
  | TINTEGER -> 4
  | TREAL -> 8
  | TBOOLEAN -> 1

let default_value = function
  | TVARCHAR i -> VARCHAR (i, "")
  | TINTEGER -> INTEGER 0l
  | TREAL -> REAL 0.0
  | TBOOLEAN -> BOOLEAN false

let write_data buf pos = function
  | VARCHAR (n,s) ->
      let ns = String.length s in
      if ns > n then Bytes.blit_string s 0 buf pos n
        else Bytes.blit_string s 0 buf pos ns; Bytes.set buf (pos + ns) '\x00'
  | INTEGER i -> Bytes.set_int32_le buf pos i
  | REAL f -> Bytes.set_int64_le buf pos (Int64.bits_of_float f)
  | BOOLEAN b -> Bytes.set buf pos (if b then '\x01' else '\x00')

let read_data buf pos = function
  | TVARCHAR n ->
      let rec read_string i =
        if Bytes.get buf (pos + i) = '\x00' || i = n then String.sub (Bytes.to_string buf) pos i
        else read_string (i + 1)
      in VARCHAR (n, read_string 0)
  | TINTEGER -> INTEGER (Bytes.get_int32_le buf pos)
  | TREAL -> REAL (Int64.float_of_bits (Bytes.get_int64_le buf pos))
  | TBOOLEAN -> BOOLEAN (Bytes.get buf pos = '\x01')

let get_data_type = function
  | VARCHAR (n,_) -> TVARCHAR n
  | INTEGER _ -> TINTEGER
  | REAL _ -> TREAL
  | BOOLEAN _ -> TBOOLEAN

module type RAW_DATA_TYPE = sig
  
  val data_typs : data_type list

end

module DataRawElt (T : RAW_DATA_TYPE) = struct
  
  type t = data array

  let byte_len = List.fold_left (fun acc typ -> acc + data_type_size typ) 0 T.data_typs
  let default = T.data_typs |> List.map default_value |> Array.of_list

  let write buf pos data =
    let final_pos = Array.fold_left (fun pos d -> write_data buf pos d; pos + data_type_size (get_data_type d)) pos data in
    if final_pos <> pos + byte_len then raise Invalid_type
  
  let read buf pos =
    let arr = Array.copy default and cursor = ref pos in
    for i = 0 to Array.length arr - 1 do
      let typ = get_data_type arr.(i) in
      arr.(i) <- read_data buf !cursor typ;
      cursor := !cursor + data_type_size typ
    done;
    arr

end

let make_data_raw_array dtl =
  let (module Raw_data_typ) = (module struct let data_typs = dtl end : RAW_DATA_TYPE) in
  let (module Raw_elt) = (module DataRawElt (Raw_data_typ) : Raw_array.RawElt with type t = data array) in
  (module Raw_array.Make (Raw_elt) : Raw_array.S with type elt = data array)


(* -- Indexs -- *)

module TreeOptions = struct
  
  type key = data
  type value = int

  let order = 4

  let compare d1 d2 =
    if d1 <: d2 then -1
    else if d1 =: d2 then 0
    else 1
end

module type DATA_TYP = sig
  val typ : data_type
end

module RawData (Typ : DATA_TYP) = struct
  
  type t = data
  let byte_len = data_type_size Typ.typ
  let default = default_value Typ.typ

  let write buf pos data =
    write_data buf pos data

  let read buf pos =
    read_data buf pos Typ.typ

end

module RawInt = struct
  
  type t = int
  let byte_len = 4
  let default = 0
  let write by pos i = Bytes.set_int32_le by pos (Int32.of_int i)
  let read by pos = Bytes.get_int32_le by pos |> Int32.to_int

end

let get_bptree_module typ =
  let (module DataTyp) = (module struct let typ = typ end : DATA_TYP) in
  let (module RawData) = (module RawData (DataTyp) : Raw_array.RawElt with type t = data) in
  (module Bptrees.Make (TreeOptions) (RawData) (RawInt) : Bptrees.S with type key = data and type value = int)
