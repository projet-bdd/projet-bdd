open Data

module MapString = Map.Make(String)
module FileTable = File.RawTable

type constr =
  | PRIMARY_KEY
  | FOREIGN_KEY
  | UNIQUE

type index = Ind : { bptrees : (module Bptrees.S with type t = 'a and type key = data and type value = int); index : 'a} -> index
type attribute_type = { a_name : string; a_type : data_type; a_constr : constr list; mutable display_name : string }
type attribute = { typ : attribute_type; position : int; index : index option }
type table = {t_name: string; file: FileTable.t; attributes : (string * attribute) list}

type meta_info = {n_tuples : int; n_attributes : int; attributes_size : int list; primary_index : attribute option}

let tbls : (string, table) Hashtbl.t = Hashtbl.create 10
let metadata_filename = "metadata"

(* STRING CONVERTERS *)
let constr_of_string = function
  | "PRIMARY_KEY" -> PRIMARY_KEY
  | "FOREIGN_KEY" -> FOREIGN_KEY
  | "UNIQUE" -> UNIQUE
  | s -> invalid_arg ("invalid constr " ^ s)
let constr_to_string = function
  | PRIMARY_KEY -> "PRIMARY_KEY"
  | FOREIGN_KEY -> "FOREIGN_KEY"
  | UNIQUE -> "UNIQUE"

let attribute_type_of_string s =
  match String.split_on_char ';' s with
  | a_name :: typ_s :: constr_s_l ->
    let a_type = data_typ_of_string typ_s in
    let a_constr = constr_s_l |> List.filter (fun s -> s <> "") |> List.map constr_of_string in
      {a_name; a_type; a_constr; display_name = a_name}
  | _ -> failwith "invalid attribute type"
let attribute_type_to_string att = att.a_name ^ ";" ^ (data_typ_to_string att.a_type) ^ ";" ^ (List.map constr_to_string att.a_constr |> String.concat ";")

let (=@) att1 att2 = att1.position = att2.position && att1.typ.a_name = att2.typ.a_name && att1.typ.a_type = att2.typ.a_type && att1.typ.a_constr = att2.typ.a_constr


(* LOAD TABLES *)
let load_tables filename =
  if Sys.file_exists filename then
    let ic = open_in filename in
    let rec loop () =
      try
        let line = input_line ic in
        let tokens = String.split_on_char ',' line in
        match tokens with
        | t_name :: filename :: attr ->
          let attr_t = List.map attribute_type_of_string attr in
          let attributes = List.mapi (fun i typ ->
            if List.mem PRIMARY_KEY typ.a_constr then
              let (module Bptree) = get_bptree_module typ.a_type in
              let ind = Ind { bptrees = (module Bptree); index = Bptree.create (t_name ^ "." ^ typ.a_name ^ ".bptree") } in
              {typ; position = i; index = Some ind}
            else {typ; position = i; index = None}) attr_t in
          let file = FileTable.create filename (attributes |> List.map (fun a -> a.typ.a_type)) false in
          Hashtbl.add tbls t_name {t_name; file; attributes = List.map (fun a ->  a.typ.a_name, a) attributes};
          loop ()
        | _ -> loop () (* invalid line *)
      with End_of_file -> ()
    in loop ();
    close_in ic

let append_table_metadata filename t =
  let oc = open_out_gen [Open_wronly; Open_append; Open_creat] 0o666 filename in
  let line = t.t_name ^ "," ^ (FileTable.filename t.file) ^ "," ^ (t.attributes |> List.map (fun (_, a) -> attribute_type_to_string a.typ) |> String.concat ",") ^ "\n" in
  output_string oc line;
  close_out oc



(* --- INTERFACE --- *)

(* TABLES *)
let create_table t_name att_t =
  if Hashtbl.mem tbls t_name then ()
  else
    let filename = t_name ^ ".tbl" in
    let attributes = List.mapi (fun i typ -> 
      if List.mem PRIMARY_KEY typ.a_constr then
        let (module Bptree) = get_bptree_module typ.a_type in
        let ind = Ind { bptrees = (module Bptree); index = Bptree.create (t_name ^ "." ^ typ.a_name ^ ".bptree") } in
        {typ = typ; position = i; index = Some ind}
      else {typ = typ; position = i; index = None}) att_t in
    let file = FileTable.create filename (attributes |> List.map (fun a -> a.typ.a_type)) false in
    let tbl = {t_name; file; attributes = List.map (fun a -> a.typ.a_name, a) attributes} in
    append_table_metadata metadata_filename tbl;
    Hashtbl.add tbls t_name tbl

let get_new_table_name =
  let counter = ref 0 in
  fun () -> incr counter; "tmp" ^ (string_of_int !counter)

let create_tmp_table att_t =
  let t_name = get_new_table_name () in
  let attributes = List.mapi (fun i typ ->
    if List.mem PRIMARY_KEY typ.a_constr then
      let (module Bptree) = get_bptree_module typ.a_type in
      let ind = Ind { bptrees = (module Bptree); index = Bptree.create (t_name ^ "." ^ typ.a_name ^ ".bptree") } in
      {typ = typ; position = i; index = Some ind}
    else {typ = typ; position = i; index = None}) att_t in
  let file = FileTable.create "" (attributes |> List.map (fun a -> a.typ.a_type)) true in
  let tbl = {t_name; file; attributes = List.map (fun a -> a.typ.a_name, a) attributes} in
  tbl


let get_table t_name = Hashtbl.find tbls t_name

let get_table_name tbl = tbl.t_name

let get_table_data tbl = FileTable.get_iterator tbl.file

let get_table_nth_data tbl n = FileTable.get tbl.file n

let get_table_attributes tbl = tbl.attributes |> List.split |> snd


(* ATTRIBUTE TYPES *)
let create_attribute_type a_name a_type a_constr = {a_name; a_type; a_constr; display_name = a_name}
let get_attribute_type_name att = att.a_name
let get_attribute_type_data_type att = att.a_type
let get_attribute_type_constraints att = att.a_constr
let update_attribute_type_display_name att name = att.display_name <- name

(* ATTRIBUTE *)
let get_attribute tbl att_name = List.assoc att_name tbl.attributes
let get_attribute_type att = att.typ
let get_attribute_position att = att.position
let get_attribute_index att = att.index



(* METADATA *)
let table_meta_info tbl = {
  n_tuples = FileTable.length tbl.file;
  n_attributes = List.length tbl.attributes;
  attributes_size = List.map (fun (_,a) -> data_type_size a.typ.a_type) tbl.attributes;
  primary_index = tbl.attributes |> List.find_opt (fun (_,a) -> List.mem PRIMARY_KEY a.typ.a_constr) |> Option.map snd
}

(* INSERT *)
let insert_row tbl datas =
  let info = table_meta_info tbl in
  (match info.primary_index with
    | None -> ()
    | Some primary_index ->
      let Ind { bptrees = (module BPTree); index} = primary_index.index |> Option.get in
      BPTree.insert index datas.(primary_index.position) info.n_tuples);
  FileTable.append_data tbl.file datas

(* INIT *)
let () = load_tables metadata_filename



let print_table tbl =
  Printf.printf "\n\nTable %s :\n" tbl.t_name;
  let num_att = List.length tbl.attributes in
  let attr_str = List.map (fun (_, a) ->
    let n = a.typ.display_name in
    let n_str = String.length n in
    let n_pad_1 = (20 - n_str) / 2 in
    let n_pad_2 = 20 - n_str - n_pad_1 in
    let padding_1 = String.make n_pad_1 ' ' in
    let padding_2 = String.make n_pad_2 ' ' in
      if n_str >= 20 then String.sub n 0 20
      else padding_1 ^ n ^ padding_2) tbl.attributes
  in
  let print_separator () =
    Printf.printf "+";
    for _ = 0 to num_att-1 do
      for _ = 1 to 20 do Printf.printf "-"; done;
      Printf.printf "+"
    done;
    Printf.printf "\n"
  in
  print_separator ();
  Printf.printf "|";
  List.iter (fun s -> Printf.printf "%s|" s) attr_str;
  Printf.printf "\n";
  print_separator ();
  let it = get_table_data tbl in
  let print_data data =
    Printf.printf "|";
    Array.iter (fun d ->
      let str = string_of_data d in
      let str_len = String.length str in
      let pad_1 = (20 - str_len) / 2 in
      let pad_2 = 20 - str_len - pad_1 in
      let padding_1 = String.make pad_1 ' ' in
      let padding_2 = String.make pad_2 ' ' in
      if str_len >= 20 then Printf.printf " %s|" (String.sub str 0 20)
      else Printf.printf "%s%s%s|" padding_1 str padding_2) data
  in
  let rec loop () =
    match it () with
    | Some data -> print_data data; Printf.printf "\n"; loop ()
    | None -> ()
  in loop ();
  print_separator ()
