open Data

module type FileTable = sig

  (** The type of the table reader / writer *)
  type t

  (** [create filename data_types temporary] creates a new table with the given
      filename, data types and temporary flag. The temporary flag effect depends on the implementation. *)
  val create : string -> data_type list -> bool -> t

  (** [filname t] returns the filename of the table [t]. *)
  val filename : t -> string

  (** [temporary t] returns the temporary flag of the table [t]. *)
  val temporary : t -> bool

  (** [length t] returns the number of rows stored in the table [t]. *)
  val length : t -> int

  (** [get_iterator t] returns an iterator over the rows of the table [t].
      When an iterator has reached the end, it returns None *)
  val get_iterator : t -> unit -> data array option

  (** [get t row] returns the row number [row] of the table [t]. *)
  val get : t -> int -> data array

  (** [append_data t data] appends the given data to the table [t]. *)
  val append_data : t -> data array -> unit

end

(** The naive implementation of a table using a CSV file *)
module CSVTable : FileTable

(** The efficient implementation of a table using binary files *)
module RawTable : FileTable
