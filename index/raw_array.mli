(** The signature of an [Raw_array.S] element. *)
module type RawElt = sig
  (** The type of the element stored in the array. *)
  type t

  (** The number of bytes used to store the element. *)
  val byte_len : int

  (** The default value of the element. *)
  val default : t

  (** [write buf ofs x] writes the element [x] at the position [ofs] in the buffer [buf]. *)
  val write : bytes -> int -> t -> unit

  (** [read buf ofs] reads an element from the position [ofs] in the buffer [buf]. *)
  val read : bytes -> int -> t
end

(** The signature of an Raw_array module. *)
module type S = sig

  (** An element of the array *)
  type elt

  (** The array type. It needs to be considered like a [elt array] *)
  type t


  (** [length t] returns the number of elements in [t]. *)
  val length : t -> int

  (** [filename t] returns the filename used to store the data on the disk (if the data is in the RAM, the filename doesn't mean anything but won't raise any error) *)
  val filename : t -> string

  (** [get t i] returns the [i]-th element of [t]. *)
  val get : t -> int -> elt

  (** [set t i x] sets the [i]-th element of [t] to [x]. *)
  val set : t -> int -> elt -> unit

  (** [make ?filename n] creates a new array of length [n]. If filename is not specified then the array is temporary *)
  val make : ?filename:(string option) -> int -> t

  (** [is_temporary t] returns true if [t] is temporary (ie. not persistent & in RAM if possible) *)
  val is_temporary : t -> bool

  (** [append t x] appends [x] to the end of [t]. *)
  val append : t -> elt -> unit
end

(** The functor to create an [Raw_array.S] module. *)
module Make (Elt : RawElt) : S with type elt = Elt.t
