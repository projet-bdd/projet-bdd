(** A module OPTI that hide every optimisation algorithms *)
module OPTI : sig

  type query
  type formula

end

(** This function do the following :
        - REcompile a query
        - optimise it
        - find an evaluation plan
        - execute it
        - rename the displayed attributes *)
val exec : Compil.query -> bool -> Index.table

(** Some pretty printer, in this state they are not usable :( TODO a debug mode ? *)
val pretty_print_query : OPTI.query -> (int * (string * string)) list -> unit

val _print_test : Compil.query -> unit