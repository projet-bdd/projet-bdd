(** This module is private to the optimisation group, other groups should only use the module Compil_query *)

(** All the required modules signatures are intricated : Algo contains Formula, which contains Table, which contains Attribute *)

type metainfo = Index.meta_info

(** A module for identities which just contain a special equality *)
module type Ident = sig

  type t
  val eq : t -> t -> bool

end

(** A module for attributes of a table *)
module type Attribute = sig

  module ID : Ident
  type t

end

(** A module for tables of a database
   [type map_id] encode associative lists from idents to their ocaml representations, at runtime, it is essential to remap all idents each time a new_table is created 
   [val get_id_attribute] also associate an ident to its ocaml representation, this one is persistent and is computed during compilation, it only point towards the right objects before run time, and point on non temporary tables *)
module type Table = sig

  module ID : Ident
  type t

  val info : t -> metainfo
  val taille : metainfo -> int

  module ATTRIBUTE : Attribute 
  type map_id = (ATTRIBUTE.ID.t * ATTRIBUTE.t) list
  val get_id_attribute : ATTRIBUTE.ID.t -> ATTRIBUTE.t

end

(** A module for formulas appearing in the query and algorithms *)
module type Formula = sig

  module TABLE : Table
  type attribute_id = TABLE.ATTRIBUTE.ID.t

  type comp = | Eq | Diff | Inf | InfEq | Sup | SupEq

  type comp_expr =
    | Attr of TABLE.ATTRIBUTE.ID.t
    | Data of Data.data

  type t =
    | Comp of comp * comp_expr * comp_expr
    | And of t * t
    | Or of t * t

  val eval_formula : t -> TABLE.map_id -> Data.data array -> bool

end

(** A module to group what is needed by the algorithms *)
module type Algo = sig
 
  module FORMULA : Formula
  type formula = FORMULA.t
  type attribute_id = FORMULA.TABLE.ATTRIBUTE.ID.t
  type map_id = FORMULA.TABLE.map_id
  type table = FORMULA.TABLE.t
  type t

  type algo_type =
    | PROJ_algo of attribute_id list
    | RESTRICT_algo of formula
    | CROSS_algo
    | UNION_algo
    | DIFF_algo
    | JOIN_algo of formula


  val algo_list : algo_type -> t list
  val complexity : t -> metainfo -> int
  val eval1 : t -> (map_id * table) -> (map_id * table)
  val eval2: t -> (map_id * table) -> (map_id * table) -> (map_id * table)
end 

(** We expose the following functor, that gives anything required for queries once an Algo module is given *)
module QUERY_OPTI (ALGO : Algo) : sig
  
  open ALGO
  type algo = ALGO.t
  open ALGO.FORMULA
  type formula = FORMULA.t
  open ALGO.FORMULA.TABLE
  type table = TABLE.t
  type attribute_id = ATTRIBUTE.ID.t

  type query =
    | RELATION of (table * attribute_id list)
    | PROJ of query * attribute_id list
    | RESTRICT of query * formula
    | CROSS of query * query 
    | UNION of query * query 
    | DIFF of query * query 
    | JOIN of query * query * formula

  val select_descent : query -> query
  val projection_descent : query -> query

  val remap_join_attribute : query -> query
 
  type eval_plan =
    | Leaf of ((table * attribute_id list) * metainfo)
    | Node1 of (eval_plan * algo * metainfo) 
    | Node2 of (eval_plan * eval_plan * algo * metainfo)

  val find_plan : query -> float -> (eval_plan * int)

  val plan_complexity : eval_plan -> int

  val run : eval_plan -> (map_id * table)

end
