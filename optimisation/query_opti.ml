(* --------------------------------------------------------------------------------------------- *)
(* --------------------------------------------------------------------------------------------- *)
(* ----------                 Modules necessaire pour l'opti de query                 ---------- *)
(* --------------------------------------------------------------------------------------------- *)
(* --------------------------------------------------------------------------------------------- *)



(* type from Index, not use through a module so field's name should not be modified *)
type metainfo = Index.meta_info

(* module for identifiant with more general equality *)
module type Ident = sig

  type t
  val eq : t -> t -> bool

end

(* module for table's attributes *)
module type Attribute = sig

  module ID : Ident
  type t

end

(* module for database's tables *)
module type Table = sig

  module ID : Ident
  type t

  val info : t -> metainfo
  val taille : metainfo -> int

  module ATTRIBUTE : Attribute
  type map_id = (ATTRIBUTE.ID.t * ATTRIBUTE.t) list
  val get_id_attribute : ATTRIBUTE.ID.t -> ATTRIBUTE.t

end

(* module for formulas used in the queries and algorithms *)
module type Formula = sig

  module TABLE : Table
  type attribute_id = TABLE.ATTRIBUTE.ID.t

  type comp = | Eq | Diff | Inf | InfEq | Sup | SupEq

  type comp_expr =
    | Attr of attribute_id
    | Data of Data.data

  type t =
    | Comp of comp * comp_expr * comp_expr
    | And of t * t
    | Or of t * t

  val eval_formula : t -> TABLE.map_id -> Data.data array -> bool

end

(* module for the algorithms that execute a query *)
module type Algo = sig

  module FORMULA : Formula
  type formula = FORMULA.t
  type attribute_id = FORMULA.TABLE.ATTRIBUTE.ID.t
  type map_id = FORMULA.TABLE.map_id
  type table = FORMULA.TABLE.t

  type t

  (* TODO more algorithm type should be implemented as "sort" ou "distinct" *)
  type algo_type =
    | PROJ_algo of attribute_id list 
    | RESTRICT_algo of formula 
    | CROSS_algo  
    | UNION_algo 
    | DIFF_algo 
    | JOIN_algo of formula 

  val algo_list : algo_type -> t list
  val complexity : t -> metainfo -> int
  val eval1 : t -> (map_id * table) -> (map_id * table)
  val eval2 : t -> (map_id * table) -> (map_id * table) -> (map_id * table)

end 

(* --------------------------------------------------------------------------------------------- *)
(* --------------------------------------------------------------------------------------------- *)
(* ----------        Module de sortie si toutes les dépendences sont fournies        ----------- *)
(* --------------------------------------------------------------------------------------------- *)
(* --------------------------------------------------------------------------------------------- *)



module QUERY_OPTI (ALGO : Algo) = struct

  (* ouverture et renommages des types importants pour plus de lisibilité *)
  open ALGO
  type algo = ALGO.t
  open ALGO.FORMULA
  type formula = FORMULA.t
  open ALGO.FORMULA.TABLE
  type table = TABLE.t
  type attribute_id = ATTRIBUTE.ID.t

  type query =
    | RELATION of (table * attribute_id list)
    | PROJ of query * attribute_id list
    | RESTRICT of query * formula
    | CROSS of query * query 
    | UNION of query * query 
    | DIFF of query * query 
    | JOIN of query * query * formula

  (* -------------------------------------------- *)
  (* ----- Fonctions manipulation attributs ----- *)
  (* -------------------------------------------- *)

  let rec mem_attr (l : attribute_id list) (a : attribute_id) : bool =
    match l with
    | [] -> false
    | h :: _ when ATTRIBUTE.ID.eq h a -> true
    | _ :: q -> mem_attr q a

  let include_attr (l1 : attribute_id list) (l2 : attribute_id list) : bool =
    List.fold_left (fun res a -> res && (mem_attr l2 a)) true l1
  

  let rec ids_from_q (q : query) : attribute_id list =
    match q with
    | RELATION (_, ids) | PROJ (_, ids)                   -> ids
    | RESTRICT (q', _)  | UNION (q', _)    | DIFF (q', _) -> ids_from_q q'
    | CROSS (q1, q2)    | JOIN (q1, q2, _)                -> List.append (ids_from_q q1) (ids_from_q q2)

  let rec ids_from_f (f : formula) : attribute_id list =
    match f with
    | Comp (_, Attr a, Attr b) -> [a; b]
    | Comp (_, Attr a, _) | Comp (_, _, Attr a) -> [a]
    | Comp (_, _, _) -> []
    | And (f1, f2) | Or (f1, f2) ->
        let ids_f1 = (ids_from_f f1) in
        List.append ids_f1 (List.filter (mem_attr ids_f1) (ids_from_f f2))

  let rec split_formula_clauses (f : formula) : formula list =
    match f with
    | Comp (_, _, _) -> [f]
    | And (f1, f2) -> List.append (split_formula_clauses f1) (split_formula_clauses f2)
    | Or (f1, f2) ->
        let cnf1 = split_formula_clauses f1 in
        let cnf2 = split_formula_clauses f2 in
        List.flatten (List.map (fun c1 -> (List.map (fun c2 -> Or (c1, c2)) cnf2)) cnf1)
  
  let rec flatten_cnf (cnf : formula list) : formula =
    match cnf with
    | [] -> failwith "should not happen, empty cnf"
    | [f] -> f
    | h :: q -> And (h, flatten_cnf q)


  (* -------------------------------------------- *)
  (* -----     optimisation algébrique     ------ *)
  (* -------------------------------------------- *)

  (* -- Gestion des jointures -- *)

  (* let rec clean (phi: formula) : formula = *)
  (*   match phi with *)
  (*   | And(True, f) | And(f, True) -> clean f *)
  (*   | Or(True,f)   | Or(f,True)   -> clean f *)
  (*   | And(f1,f2) -> And(clean f1, clean f2) *)
  (*   | Or(f1,f2)  -> Or(clean f1, clean f2) *)
  (*   | _ -> phi *)
  (* *)
  (* let rec join_split (q: query) : query = *)
  (*   match q with *)
  (*   | JOIN (q1,q2,phi) -> *)
  (*     (match phi with *)
  (*     | True -> CROSS(join_split q1, join_split q2) *)
  (*     | And (f1,f2) -> RESTRICT(join_split (JOIN(join_split q1, join_split q2, f2)), f1) *)
  (*     | Or(f1,f2) -> UNION(join_split (JOIN(join_split q1, join_split q2, f1)), join_split (JOIN(join_split q1, join_split q2, f2))) *)
  (*     | Eq _ -> JOIN(join_split q1, join_split q2, phi) *)
  (*     ) *)
  (*   | RELATION t -> RELATION t *)
  (*   | PROJ (q1, l) -> PROJ(join_split q1, l) *)
  (*   | RESTRICT (q1,phi) -> RESTRICT(join_split q1, phi) *)
  (*   | CROSS (q1, q2) -> CROSS(join_split q1, join_split q2) *)
  (*   | UNION (q1, q2) -> UNION(join_split q1, join_split q2) *)
  (*   | DIFF (q1, q2) -> DIFF(join_split q1, join_split q2) *)

  (* first clauses are joinable, second are not *)
  let rec split_joinable_clauses (cnf : formula list) (ids1 : attribute_id list) (ids2 : attribute_id list) : (formula list) * (formula list) =
    match cnf with
    | [] -> [], []
    | (Comp (Eq, Attr i1, Attr i2)) as h :: q ->
        let (joinable, non_joinable) = split_joinable_clauses q ids1 ids2 in
        if (List.mem i1 ids1) && (List.mem i2 ids2) then
          (h :: joinable, non_joinable)
        else if (List.mem i1 ids2) && (List.mem i2 ids1) then
          ((Comp (Eq, Attr i2, Attr i1)) :: joinable, non_joinable)
        else
          (joinable, h :: non_joinable)
    | h :: q ->
        let (joinable, non_joinable) = split_joinable_clauses q ids1 ids2 in
        (joinable, h :: non_joinable)

  (* -- Gestion des restrictions -- *)

  (* theses two functions are mutualy recursive, the first one looks for "select", the second one cascade it the lower possible *)
  let rec select_descent (q : query) : query =
    match q with
    | RESTRICT (q', phi) -> select_cascade (split_formula_clauses phi) q' 
    (* otherwise just look for select *)
    | RELATION r -> RELATION r
    | PROJ (q', l) -> PROJ (select_descent q', l)
    | CROSS (q1, q2) -> CROSS (select_descent q1, select_descent q2)
    | UNION (q1, q2) -> UNION (select_descent q1, select_descent q2)
    | DIFF (q1, q2) -> DIFF (select_descent q1, select_descent q2)
    | JOIN (q1, q2, phi) -> select_cascade (split_formula_clauses phi) (CROSS (q1, q2)) 

  and select_cascade (cnf : formula list) (q : query) : query = 
    match q with
    | RELATION r ->
        (match cnf with
        | [] -> RELATION r
        | _  -> RESTRICT (RELATION r, flatten_cnf cnf))
    | RESTRICT (q', phi') -> select_cascade (List.append cnf (split_formula_clauses phi')) q'
    | CROSS (q1, q2) ->
        let ids1 = ids_from_q q1 in
        let ids2 = ids_from_q q2 in
        let cnf1 = List.filter (fun c -> include_attr (ids_from_f c) ids1) cnf in
        let cnf2 = List.filter (fun c -> include_attr (ids_from_f c) ids2) cnf in
        let cnf_others = List.filter (fun c -> not ((include_attr (ids_from_f c) ids1) || (include_attr (ids_from_f c) ids2))) cnf in
        let (joinable, non_joinable) = split_joinable_clauses cnf_others ids1 ids2 in
        (match joinable, non_joinable with
        | [], [] -> CROSS (select_cascade cnf1 q1, select_cascade cnf2 q2)
        | _ , [] -> JOIN (select_cascade cnf1 q1, select_cascade cnf2 q2, flatten_cnf joinable)
        | [], _  -> RESTRICT (CROSS (select_cascade cnf1 q1, select_cascade cnf2 q2), flatten_cnf non_joinable)
        | _ , _  -> RESTRICT (JOIN (select_cascade cnf1 q1, select_cascade cnf2 q2, flatten_cnf joinable), flatten_cnf non_joinable))
    | JOIN (q1, q2, phi') -> select_cascade (List.append cnf (split_formula_clauses phi')) (CROSS (q1, q2)) (* reutilise le cas précédent *)
    (* other wise simply cascade the formula *)
    | PROJ (q', l) -> PROJ (select_cascade cnf q', l)
    | UNION (q1, q2) -> UNION (select_cascade cnf q1, select_cascade cnf q2)
    | DIFF (q1, q2) -> DIFF (select_cascade cnf q1, select_cascade cnf q2)

  (* -- gestion des jointures --*)

  (* lorsque il y a une jointure, les attributs de la jointure provenant de la table de gauche sont identifiés avec certains attributs de la table de droite, 
     On a besoin, pour simplifier l'association des identifiants aux attributs (remaping de compil_query) que les identifiants au dessus d'une jointure, pointent toujours vers l'identifiant qui provient de la droite, c'est exactement ce que font ces fonctions et rien de plus *)
  (* il aurrait surement été plus inteligent de modifier l'implementation des remaping des identifiant vers les attributs "TODO" *)

  let rec get_join_attribute_id_list (phi : formula) = match phi with
    | Comp (Eq, Attr i1, Attr i2) -> [i1], [i2]
    | And (phi1, phi2) ->
        let (l11, l12) = get_join_attribute_id_list phi1 in
        let (l21, l22) = get_join_attribute_id_list phi2 in
        (List.append l11 l21, List.append l12 l22)
    | _ -> failwith "fail at joining the correct attribute, something wrong with optimisation, sorry"

  let rec rename_attribute_id (i : attribute_id) (rename : (attribute_id * attribute_id) list) : attribute_id =
    match rename with
    | [] -> i
    | (j, j_new) :: _ when ATTRIBUTE.ID.eq i j -> j_new
    | _ :: q -> rename_attribute_id i q

  let rec rename_formula (phi : formula) (rename : (attribute_id * attribute_id) list) : formula =
    match phi with
    | Comp(op, Attr i1, Attr i2) -> Comp(op, Attr (rename_attribute_id i1 rename), Attr (rename_attribute_id i2 rename))
    | Comp(op, Attr i, x) -> Comp(op, Attr (rename_attribute_id i rename), x)
    | Comp(op, x, Attr i) -> Comp(op, x, Attr (rename_attribute_id i rename))
    | Comp(_, _, _) -> phi
    | And (phi1, phi2) -> And (rename_formula phi1 rename, rename_formula phi2 rename)
    | Or (phi1, phi2) -> Or (rename_formula phi1 rename, rename_formula phi2 rename)

  let remap_join_attribute (query : query) : query =
    let rec aux (q : query) : ((attribute_id * attribute_id) list) * query =
      match q with
      | RELATION r -> [], RELATION r
      | JOIN (q1, q2, phi) ->
          let (renames1, q1') = aux q1 in
          let (renames2, q2') = aux q2 in
          let old_renames = List.append renames1 renames2 in
          let new_phi = rename_formula phi old_renames in
          let (join1, join2) = get_join_attribute_id_list new_phi in
          let new_renames = List.combine join1 join2 in
          (List.append new_renames old_renames), (JOIN (q1', q2', new_phi))
      | RESTRICT (q', phi) ->
          let (renames, new_q) = aux q' in
          renames, (RESTRICT (new_q, rename_formula phi renames))
      | CROSS (q1, q2) ->
          let (renames1, q1') = aux q1 in
          let (renames2, q2') = aux q2 in
          (List.append renames1 renames2), (CROSS (q1', q2'))
      | PROJ (q', l) ->
          let (renames, new_q) = aux q' in
          renames, (PROJ (new_q, List.map (fun i -> rename_attribute_id i renames) l))
      | UNION (q1, q2) ->
          let (renames1, q1') = aux q1 in
          let (renames2, q2') = aux q2 in
          (List.append renames1 renames2), (UNION (q1', q2'))
      | DIFF (q1, q2) -> 
          let (renames1, q1') = aux q1 in
          let (renames2, q2') = aux q2 in
          (List.append renames1 renames2), (DIFF (q1', q2'))
    in snd (aux query)


  (* -- Gestion des projections -- *)

  let rec projection_descent (q : query) : query =
    match q with
    | PROJ (q', l) -> projection_cascade q' l
    | RELATION _ | RESTRICT (RELATION _, _) -> q 
    | RESTRICT (q', phi) -> RESTRICT (projection_descent q', phi)
    | CROSS (q1, q2) -> CROSS (projection_descent q1, projection_descent q2)
    | UNION (q1, q2) -> UNION (projection_descent q1, projection_descent q2)
    | DIFF (q1, q2) -> DIFF (projection_descent q1, projection_descent q2)
    | JOIN (q1, q2, phi) -> JOIN (projection_descent q1, projection_descent q2, phi)

  (* attention cette fonction induit parfois des cas de projections sur des attributs qui existent pas *)
  and projection_cascade (q : query) (l : attribute_id list) : query =
    match q with
    | RELATION _ | RESTRICT (RELATION _, _) -> PROJ (q, l)
    | PROJ (q', _) -> projection_cascade q' l
    | CROSS (q1, q2) ->
        let l1 = List.filter (mem_attr (ids_from_q q1)) l in
        let l2 = List.filter (mem_attr (ids_from_q q2)) l in
        CROSS (projection_cascade q1 l1, projection_cascade q2 l2)
    | JOIN (q1, q2, f) ->
        let lf = ids_from_f f in
        let l_q1 = ids_from_q q1 in
        let l_q2 = ids_from_q q2 in
        (* attributes that are in f but not in l *)
        let lf_q1 = (List.filter (fun a -> (mem_attr l_q1 a) && not (mem_attr l a)) lf) in
        let lf_q2 = (List.filter (fun a -> (mem_attr l_q2 a) && not (mem_attr l a)) lf) in
        (* attributes from qi and from f *)
        let l1 = List.append (List.filter (mem_attr l_q1) l) lf_q1 in
        let l2 = List.append (List.filter (mem_attr l_q2) l) lf_q2 in
        if (lf_q1 = []) && (lf_q2 = []) then
          JOIN (projection_cascade q1 l1, projection_cascade q2 l2, f)
        else
          PROJ (JOIN (projection_cascade q1 l1, projection_cascade q2 l2, f), l)
    | RESTRICT (q', f) ->
        let lf = ids_from_f f in
        let lf_q = List.filter (fun a -> not (mem_attr l a)) lf in
        if lf_q = [] then
          RESTRICT (projection_cascade q' l, f)
        else
          PROJ (RESTRICT (projection_cascade q' (List.append l lf_q), f), l)
    | UNION (q1, q2) -> UNION (projection_cascade q1 l, projection_cascade q2 l)
    | DIFF (q1, q2) -> PROJ (DIFF (q1, q2), l) (* cannot go lower *)
 

  (* ----------------------------------------- *)
  (* ----- Creation du plan d'évalution ------ *)
  (* ----------------------------------------- *)


  (* TODO comment gerer les pipelines ? *)

  type eval_plan =
    | Leaf of ((table * attribute_id list) * metainfo)
    | Node1 of (eval_plan * algo * metainfo) 
    | Node2 of (eval_plan * eval_plan * algo * metainfo)

  let info_of_plan : eval_plan -> metainfo = function
    | Leaf (_,info) | Node1(_,_,info) | Node2(_,_,_,info) -> info

  (* -- Estimation des metadonnées en sortie d'un algorithme *)

  let info_of_table (t: table) =
    (TABLE.info t)

  let info_of_proj (m : metainfo) (l: attribute_id list) =
    { m with
    n_attributes = List.length l }

  let info_of_cross (m1 : metainfo) (m2 : metainfo) = 
    { m1 with
    n_tuples = m1.n_tuples * m2.n_tuples;
    n_attributes = m1.n_attributes + m2.n_attributes }

  let info_of_join (m1 : metainfo) (m2 : metainfo) (_ : formula) = 
    { m1 with
    n_tuples = m1.n_tuples * m2.n_tuples;
    n_attributes = m1.n_attributes + m2.n_attributes }

  let info_of_select (m : metainfo) (_ : formula) =
    m

  let info_of_union (m1 : metainfo) (m2 : metainfo) =
    { m1 with
    n_tuples = m1.n_tuples + m2.n_tuples }

  let info_of_diff (m1 : metainfo) (_ : metainfo) = 
    m1

  (* -- Estimation du cout d'un plan d'évaluation -- *)

  let rec plan_complexity (e: eval_plan) : int =
    match e with
    | Leaf ((t, _), _) -> TABLE.taille (TABLE.info t)
    | Node1 (plan, alg, info) -> plan_complexity plan + ALGO.complexity alg info
    | Node2 (plan1, plan2, alg, info) -> plan_complexity plan1 + plan_complexity plan2 + ALGO.complexity alg info

  (* -- Creation du plan naif (execution de la query sans changement ni choix d'algo (toujours le premier) -- *)

  let rec naive_plan (q: query) : eval_plan =
    match q with
    | RELATION t ->
        Leaf (t, info_of_table (fst t))
    | PROJ (q', l) -> 
        let e = naive_plan q' in
        let m = info_of_plan e in
        Node1 (e, List.hd (algo_list (PROJ_algo l)), info_of_proj m l )
    | CROSS (q1, q2) ->
        let e1 = naive_plan q1 in
        let m1 = info_of_plan e1 in
        let e2 = naive_plan q2 in
        let m2 = info_of_plan e2 in
        Node2 (e1, e2, List.hd (algo_list CROSS_algo), info_of_cross m1 m2)
    | JOIN (q1, q2, phi) ->
        let e1 = naive_plan q1 in
        let m1 = info_of_plan e1 in
        let e2 = naive_plan q2 in
        let m2 = info_of_plan e2 in
        Node2 (e1, e2, List.hd (algo_list (JOIN_algo phi)), info_of_join m1 m2 phi)
    | RESTRICT (q', phi) ->
        let e = naive_plan q' in
        let m = info_of_plan e in
        Node1 (e, List.hd (algo_list (RESTRICT_algo phi)), info_of_select m phi)
    | UNION (q1, q2) ->
        let e1 = naive_plan q1 in
        let m1 = info_of_plan e1 in
        let e2 = naive_plan q2 in
        let m2 = info_of_plan e2 in
        Node2 (e1, e2, List.hd (algo_list UNION_algo), info_of_union m1 m2)
    | DIFF (q1, q2) ->
        let e1 = naive_plan q1 in
        let m1 = info_of_plan e1 in
        let e2 = naive_plan q2 in
        let m2 = info_of_plan e2 in
        Node2 (e1, e2, List.hd (algo_list DIFF_algo), info_of_diff m1 m2)

  (* -- Creation de plan moins naif -- *) 

  let pick_random () (l: 'a list) =
    Random.self_init ();
    let i = Random.int (List.length l) in
    let rec aux l' i' =
      match l' with
      | [] -> failwith "error query_opti, pick_random"
      | hd::tl -> if i' = 0 then hd else aux tl (i' - 1)
    in aux l i

  let rec generate_plan (q: query)=
    match q with
    | RELATION t ->
        Leaf (t, info_of_table (fst t))
    | PROJ (q', l) -> 
        let e = generate_plan q' in
        let m = info_of_plan e in
        Node1 (e,  pick_random () (algo_list (PROJ_algo l)), info_of_proj m l )
    | CROSS (q1, q2) ->
        let e1 = generate_plan q1 in
        let m1 = info_of_plan e1 in
        let e2 = generate_plan q2 in
        let m2 = info_of_plan e2 in
        Node2 (e1, e2, pick_random () (algo_list CROSS_algo), info_of_cross m1 m2)
    | JOIN (q1, q2, phi) ->
        let e1 = generate_plan q1 in
        let m1 = info_of_plan e1 in
        let e2 = generate_plan q2 in
        let m2 = info_of_plan e2 in
        Node2 (e1, e2, pick_random () (algo_list (JOIN_algo phi)), info_of_join m1 m2 phi)
    | RESTRICT (q', phi) ->
        let e = generate_plan q' in
        let m = info_of_plan e in
        Node1 (e, pick_random () (algo_list (RESTRICT_algo phi)), info_of_select m phi)
    | UNION (q1, q2) ->
        let e1 = generate_plan q1 in
        let m1 = info_of_plan e1 in
        let e2 = generate_plan q2 in
        let m2 = info_of_plan e2 in
        Node2 (e1, e2, pick_random () (algo_list UNION_algo), info_of_union m1 m2)
    | DIFF (q1, q2) ->
        let e1 = generate_plan q1 in
        let m1 = info_of_plan e1 in
        let e2 = generate_plan q2 in
        let m2 = info_of_plan e2 in
        Node2 (e1, e2, pick_random () (algo_list DIFF_algo), info_of_diff m1 m2)

  let find_plan (q: query) (timeout: float) : (eval_plan * int) =
  let plan = ref (naive_plan q) in
  let cost = ref (plan_complexity !plan) in
  let start = Unix.time () in
  while timeout > ((Unix.time ()) -. start) do
    let p = generate_plan q in
    let c = plan_complexity p in
    if c < !cost then (plan := p; cost := c)
  done;
  (!plan, !cost)


  (* ----------------------------------------- *)
  (* ----- Execution d'un plan d'évaluation -- *)
  (* ----------------------------------------- *)


  let rec run (e: eval_plan) : (map_id * table) =
    match e with
    | Leaf ((t, attr_l), _) -> (List.map (fun i -> (i, get_id_attribute i)) attr_l, t)
    | Node1 (e1, alg, _) -> eval1 alg (run e1)
    | Node2 (e1, e2, alg, _) -> eval2 alg (run e1) (run e2)


  (* ------ tests ----- *)
(* let %test "select_descent" =
    let table = Index.get_table "table" in
  let q = RESTRICT ((RELATION (table, [0])),(Comp (Eq,Attr 0,Attr 0))) in 
  select_descent q = RELATION 0 *)

end


