open Index
open Data


(* --------------------------------------------------------------------------------------------- *)
(* --------------------------------------------------------------------------------------------- *)
(* ----------                 Implementation des modules necessaires                 ----------- *)
(* --------------------------------------------------------------------------------------------- *)
(* --------------------------------------------------------------------------------------------- *)


type metainfo = Index.meta_info

(* les identifiants seront simplements des entiers *)
module Id = struct

  type t = int
  let eq n p = (n = p)

end

(* les attributs sont implémentés par le groupe Index *)
module A = struct

  module ID = Id
  type t = Index.attribute

end

(* les tables sont implémentés par le groupe Index *)
module T = struct

  module ID = Id
  type t = Index.table
 
  let info : t -> metainfo = Index.table_meta_info
  let taille ( m : metainfo) : int = m.n_tuples

  module ATTRIBUTE = A
  type map_id = (ATTRIBUTE.ID.t * ATTRIBUTE.t) list (* liste associative *)
  let map_id_hashtbl : (ATTRIBUTE.ID.t, ATTRIBUTE.t) Hashtbl.t = Hashtbl.create 5
  let get_id_attribute i =
    match Hashtbl.find_opt map_id_hashtbl i with
    | Some a -> a
    | None -> failwith "fail at compilation, ill-formed attributes"

end

(* les formules sont implémentées par nous même *)
module F = struct

  module TABLE = T
  type attribute_id = TABLE.ATTRIBUTE.ID.t

  type comp = Eq | Diff | Inf | InfEq | Sup | SupEq

  type comp_expr =
    | Attr of attribute_id
    | Data of Data.data

  type t =
    | Comp of comp * comp_expr * comp_expr
    | And of t * t
    | Or of t * t

  let comp_implem (c : comp) (d1 : Data.data) (d2 : Data.data) : bool =
    match c with
    | Eq -> d1 =: d2
    | Diff -> not (d1 =: d2)
    | Inf -> d1 <: d2
    | InfEq -> not (d2 <: d1)
    | Sup -> d2 <: d1
    | SupEq -> not (d1 <: d2)

  let rec eval_formula (f : t) (map : TABLE.map_id) (d : Data.data Array.t) : bool =
    match f with
    | Comp (c, Attr i1, Attr i2) ->
      let a1 = List.assoc i1 map in
      let a2 = List.assoc i2 map in
      comp_implem c d.(Index.get_attribute_position a1) d.(Index.get_attribute_position a2)
    | And (f1, f2) ->
      let eval1 = eval_formula f1 map d in
      let eval2 = eval_formula f2 map d in
      eval1 && eval2
    | Or (f1, f2) ->
      let eval1 = eval_formula f1 map d in
      let eval2 = eval_formula f2 map d in
      eval1 || eval2
    | _ -> failwith "TODO"

end

(* les algos sont implémentés par le groupe Algo *)
(* /!\ la conversion des types est non triviale *)
module ALG = struct

  module FORMULA = F
  type formula = FORMULA.t
  type attribute_id = FORMULA.TABLE.ATTRIBUTE.ID.t
  type map_id = FORMULA.TABLE.map_id
  type table = FORMULA.TABLE.t

  type algo_type =
    | PROJ_algo of attribute_id list
    | RESTRICT_algo of formula
    | CROSS_algo
    | UNION_algo
    | DIFF_algo
    | JOIN_algo of formula

  (* Les algoithms sont associés à une fonction qui re-map les identifiants vers les nouvaux attributs formés *)
  (* Ce devrait surement etre un type record, qui regroupe :
          - l'algo
          - sa complexité
          - un remaping des attributs
  *)
  type t =
    (*| NULLARY of table*)
    | UNARY of ((map_id * table) -> (map_id * table))
    | BINARY of ((map_id * table) -> (map_id * table) -> (map_id * table)) 

  let map_binder map = (fun x -> List.assoc x map)
  let formula_binder phi map = (fun data -> FORMULA.eval_formula phi map data)

  let rec get_join_attribute_id_list (phi : formula) = match phi with
    | Comp (Eq, Attr i1, Attr i2) -> [i1], [i2]
    | And (phi1, phi2) ->
        let (l11, l12) = get_join_attribute_id_list phi1 in
        let (l21, l22) = get_join_attribute_id_list phi2 in
        (List.append l11 l21, List.append l12 l22)
    | _ -> failwith "fail at joining the correct attribute, something wrong with optimisation, sorry"

  let get_join_attribute_list (phi : formula) (map1 : map_id) (map2 : map_id) =
    let (i1, i2) = get_join_attribute_id_list phi in
    (List.map (fun i -> List.assoc i map1) i1), (List.map (fun i -> List.assoc i map2) i2)

  (* fonctions qui re-map les identifiants vers les nouvelles tables générer par les attributs *) 
  let remap_proj l _map new_t = List.combine l (Index.get_table_attributes new_t)
  let remap_restrict _phi map new_t = List.combine (fst (List.split map)) (Index.get_table_attributes new_t)
  let remap_cross map1 map2 new_t = List.combine (List.append (fst (List.split map1)) (fst (List.split map2))) (Index.get_table_attributes new_t)
  let remap_set_ope map1 _ new_t = List.combine (fst (List.split map1)) (Index.get_table_attributes new_t)
  let remap_join phi map1 map2 new_t =
    let i1 = fst (List.split map1) in
    let i2 = fst (List.split map2) in
    let (join_1, _) = get_join_attribute_id_list phi in
    let new_attributes = Index.get_table_attributes new_t in
    List.combine (List.append (List.filter (fun i -> not (List.mem i join_1)) i1) i2) new_attributes
    (* /!\ suppose que les algos ne changent pas les ordres,
       en particlier les identifiants pointant vers des attributs de la jointure pointent bien vers les attributs de la deuxieme table,
       assurer par la fonction "remap_join_attribute *)

  (* conversions des algorithms en liste d'algos du type t *)
  let algo_list (alg_t: algo_type) : t list =
    match alg_t with
    | PROJ_algo l ->
        List.map (fun alg ->
          UNARY (fun (map, t) ->
                  let new_t = alg t (List.map (map_binder map) l) in
                  (remap_proj l map new_t, new_t)))
        Algo_don.proj_list
    | RESTRICT_algo phi ->
        List.map (fun alg ->
          UNARY (fun (map, t) ->
                  let new_t = alg t (formula_binder phi map) in
                  (remap_restrict phi map new_t, new_t)))
        Algo_don.restriction_list
    | CROSS_algo ->
        List.map (fun alg ->
          BINARY (fun (map1, t1) (map2, t2) ->
                   let new_t = alg t1 t2 in
                   (remap_cross map1 map2 new_t, new_t)))
        Algo_don.prod_list
    | UNION_algo ->
        List.map (fun alg ->
          BINARY (fun (map1, t1) (map2, t2) ->
                   let new_t = alg t1 t2 in
                   (remap_set_ope map1 map2 new_t, new_t)))
        Algo_don.union_list
    | DIFF_algo ->
        List.map (fun alg ->
          BINARY (fun (map1, t1) (map2, t2) ->
                    let new_t = alg t1 t2 in
                    (remap_set_ope map1 map2 new_t, new_t)))
        Algo_don.minus_list
    | JOIN_algo phi ->
        List.map (fun alg ->
          BINARY (fun (map1, t1) (map2, t2) ->
                  let (attr_l1, attr_l2) = get_join_attribute_list phi map1 map2 in
                  let new_t = alg t1 t2 attr_l1 attr_l2 in
                  (remap_join phi map1 map2 new_t, new_t)))
        Algo_don.join_list


  (* TODO not implemented yet *)
  let complexity (_: t) (_: metainfo) = 0

  let eval1 alg table =
    match alg with
    | UNARY f -> f table
    | _ -> failwith "error eval1 de Compil_query.ml"
  let eval2 alg t1 t2 =
    match alg with
    | BINARY f -> f t1 t2
    | _ -> failwith "error eval2 de Compil_query.ml"

end


(* ----------------------------------------------------------------------------------------------- *)
(* ----------------------------------------------------------------------------------------------- *)
(* ----------              Compilation des query de Compil vers les notres              ---------- *)
(* ----------------------------------------------------------------------------------------------- *)
(* ----------------------------------------------------------------------------------------------- *)


module OPTI = Query_opti.QUERY_OPTI(ALG)

(* Hashtbl qui se souvient des attributs de Index associés aux identifiants *)
let map_id_hashtbl = ALG.FORMULA.TABLE.map_id_hashtbl

(* -- types de Compil -- *)
type pre_query = Compil.query
type pre_formula = Compil.formula

let rec transform_formula (f : pre_formula) (m : (Compil.attribut * int) list) : OPTI.formula =
  match f with
  | Eq (a1, a2) -> Comp (Eq, Attr (List.assoc a1 m), Attr (List.assoc a2 m))
  | And (f1, f2) -> And (transform_formula f1 m, transform_formula f2 m)
  | Or (f1, f2) -> Or (transform_formula f1 m, transform_formula f2 m)

let attr_id = ref 0
let new_attr_id () =
  let id = !attr_id in
  incr attr_id; id

(* /!\ effet de bord, modifie map_id_hashtbl, et renvoie la correspondance ("nom table", "nom attribut") -> identifiant attribut *)
let assign_attr_from (table_name : string) (a : Index.attribute) =
  let id = new_attr_id () in
  Hashtbl.add map_id_hashtbl id a;
  ((table_name, Index.get_attribute_type_name (get_attribute_type a)), id)

(* renommes les attributs de Compil avec les renommages de Compil *)
let rec rename_attr (r : Compil.rename) (a : Compil.attribut) =
  match r with
  | [] -> a
  | (a', b) :: _ when a' = a -> b
  | _ :: r' -> rename_attr r' a

(* /!\ Ne devrait pas fonctionner lorsqu'un attribut est duppliquer dans une projection *)
(* renvoie le bon type de query ainsi qu'une liste associant les noms d'attributs en haut de l'arbre avec leurs identifiants *)
let rec transform (q : pre_query) : ((Compil.attribut * int) list (* liste associative  *) * OPTI.query) =
  match q with
  | RENAME (q', r) ->
      let (m, transformed_q') = transform q' in
      (List.map (fun (a, i) -> (rename_attr r a, i)) m, transformed_q')
  | RENAME_TABLE (q', s) ->
      let (m, transformed_q') = transform q' in
      (List.map (fun ((_, a), i) -> ((s, a), i)) m, transformed_q') 
  | RELATION s ->
      let t = get_table s in
      let attr = get_table_attributes t in
      let map = List.map (assign_attr_from s) attr in (* liste de tout les noms d'attrbiuts de la table *)
      (map, OPTI.RELATION (t, snd (List.split map)))
  | PROJ (q', al) ->
      let (m, transformed_q') = transform q' in
      (m, OPTI.PROJ (transformed_q', List.map (fun a -> List.assoc a m) al))
  | SELECT (q', f) ->
      let (m, transformed_q') = transform q' in
      (m, OPTI.RESTRICT (transformed_q', transform_formula f m))
  | CROSS (q1, q2) ->
      let (m1, transformed_q1) = transform q1 in
      let (m2, transformed_q2) = transform q2 in
      (List.append m1 m2, OPTI.CROSS (transformed_q1, transformed_q2)) (* here we have to suppose names are distincts as expected *)
  | UNION (q1, q2) -> 
      let (m, transformed_q1) = transform q1 in
      let (_, transformed_q2) = transform q2 in (* here we have to suppose attributes are the same each sides as expected *)
      (m, OPTI.UNION (transformed_q1, transformed_q2))
  | DIFF (q1, q2) ->
      let (m, transformed_q1) = transform q1 in
      let (_, transformed_q2) = transform q2 in (* here we have to suppose attributes are the same each sides as expected *)
      (m, OPTI.DIFF (transformed_q1, transformed_q2))
  | JOIN (q1, q2, f) ->
      let (m1, transformed_q1) = transform q1 in
      let (m2, transformed_q2) = transform q2 in
      let m = List.append m1 m2 in
      (m, OPTI.JOIN (transformed_q1, transformed_q2, transform_formula f m))


(* --------------------------------------------------------------------------------------------- *)
(* --------------------------------------------------------------------------------------------- *)
(* ----------                             Pretty printers                             ---------- *)
(* --------------------------------------------------------------------------------------------- *)
(* --------------------------------------------------------------------------------------------- *)

  let _pretty_print (q : OPTI.query) : unit = 

    let rec height (q : OPTI.query) acc = match q with
      | RELATION (_table, _l) -> acc
      | PROJ (q', _l) -> height q' (acc + 1)
      | RESTRICT (q', _phi) -> height q' (acc + 1)
      | CROSS (q1, q2) -> max (height q1 (acc + 1)) (height q2 (acc + 1))
      | UNION (q1, q2) -> max (height q1 (acc + 1)) (height q2 (acc + 1))
      | DIFF (q1, q2) -> max (height q1 (acc + 1)) (height q2 (acc + 1))
      | JOIN (q1, q2, _phi) -> max (height q1 (acc + 1)) (height q2 (acc + 1))
    
    in let rec width (q : OPTI.query) = match q with
    | RELATION (_table, _l) -> 1
    | PROJ (q', _l) -> width q'
    | RESTRICT (q', _phi) -> width q'
    | CROSS (q1, q2) -> width q1 + width q2
    | UNION (q1, q2) -> width q1 + width q2
    | DIFF (q1, q2) -> width q1 + width q2
    | JOIN (q1, q2, _phi) -> width q1 + width q2
    
    in let bfs (q : OPTI.query) = 
      let qheight : int = height q 0
      in let f = Queue.create() in
      Queue.push (0, 0, q) f; (* depth, indentation, query *)
      let acc = Queue.create() in 
      let rec aux () = match Queue.take_opt f with
        | None -> ()
        | Some (i, j, RELATION t) -> Queue.push (qheight - i, j, OPTI.RELATION t) acc; aux ()
        | Some (i, j, PROJ (q', l)) -> Queue.push (qheight - i, j, OPTI.PROJ (q', l)) acc; Queue.push (i + 1, j, q') f; aux ()
        | Some (i, j, RESTRICT (q', phi)) -> Queue.push (qheight - i, j, OPTI.RESTRICT (q', phi)) acc; Queue.push (i + 1, j, q') f; aux () 
        | Some (i, j, CROSS (q1, q2))  -> Queue.push (qheight - i, j, OPTI.CROSS (q1, q2)) acc; Queue.push (i + 1, j, q1) f; Queue.push (i + 1, j + width q1, q2) f; aux ()
        | Some (i, j, UNION (q1, q2))  -> Queue.push (qheight - i, j, OPTI.UNION (q1, q2)) acc; Queue.push (i + 1, j, q1) f; Queue.push (i + 1, j + width q1, q2) f;  aux ()
        | Some (i, j, DIFF (q1, q2)) -> Queue.push (qheight - i, j, OPTI.DIFF (q1, q2)) acc; Queue.push (i + 1, j, q1) f; Queue.push (i + 1, j + width q1, q2) f; aux ()
        | Some (i, j, JOIN (q1, q2, phi)) -> Queue.push (qheight - i, j, OPTI.JOIN (q1, q2, phi)) acc; Queue.push (i + 1, j, q1) f; Queue.push (i + 1, j + width q1, q2) f; aux ()
        (*| Some (i, RENAME (q', s1, s2)) -> "TODO"*)
      in aux (); acc

    in let print_space (i : int) (space_before : int) : unit = print_string (String.make (i + 15 * space_before) ' ')

    in let qprint (q : OPTI.query) (space_before : int) = 
      match q with
      | RELATION _ -> print_space 0 space_before; print_string "RELATION "; print_space 5 0
      | PROJ _ -> print_space 0 space_before; print_string "PROJ "; print_space 11 0
      | RESTRICT _ -> print_space 0 space_before; print_string "RESTRICT"; print_space 7 0
      | CROSS _  -> print_space 0 space_before; print_string "CROSS"; print_space 10 0
      | UNION _  -> print_space 0 space_before; print_string "UNION"; print_space 10 0
      | DIFF _ -> print_space 0 space_before; print_string "DIFF"; print_space 11 0
      | JOIN _ -> print_space 0 space_before; print_string "JOIN"; print_space 11 0
      (*| RENAME of (q', s1, s2) -> print_string "RENAME"*)

    in let rec final_print (f : (int * int * OPTI.query) Queue.t) (current_depth : int) (previous_indent : int) = match Queue.take_opt f with
      | None -> print_newline ()
      | Some (i, j, q) when current_depth != i -> print_newline (); qprint q j; final_print f i j
      | Some (_, j, q) -> qprint q (j - previous_indent - 1); final_print f current_depth j

  in final_print (bfs q) (height q 0) (-1)

  let rec print_tabs (i : int) : unit =
    if i <= 0 then
      ()
    else
      (print_string ("   ");
       print_tabs (i - 1))

  let rec pretty_print_query (query : OPTI.query) (renames) = 
  let rec aux (q : OPTI.query) (n_tab : int) =
    print_tabs (n_tab);
    match q with
    | RELATION (t, _) ->
        print_string ("(" ^ (Index.get_table_name t) ^ ")");
    | PROJ (q', al) ->
        print_string "PROJ "; print_attribute_list al renames;
        print_newline ();
        aux q' (n_tab + 1)
    | RESTRICT (q', phi) ->
        print_string "RESTRICT "; print_formula phi renames;
        print_newline ();
        aux q' (n_tab + 1)
    | CROSS (q1, q2) ->
        print_string "CROSS ";
        print_newline ();
        aux q1 (n_tab + 1) ;
        print_newline ();
        aux q2 (n_tab + 1)
    | UNION (q1, q2) ->
        print_string "UNION ";
        print_newline ();
        aux q1 (n_tab + 1) ;
        print_newline ();
        aux q2 (n_tab + 1)
    | DIFF (q1, q2) -> 
        print_string "DIFF ";
        print_newline ();
        aux q1 (n_tab + 1) ;
        print_newline ();
        aux q2 (n_tab + 1)
    | JOIN (q1, q2, phi) ->
        print_string "JOIN "; print_formula phi renames;
        print_newline ();
        aux q1 (n_tab + 1) ;
        print_newline ();
        aux q2 (n_tab + 1)
  in aux query 0; print_newline ()

and print_formula (phi : OPTI.formula) (renames) = match phi with
  | Comp (op, e1, e2) ->
      print_comp_expr e1 renames; 
      print_comp op;
      print_comp_expr e2 renames

  | And (phi1, phi2) ->
      print_char '(';
      print_formula phi1 renames;
      print_string " ∧ ";
      print_formula phi2 renames;
      print_char ')'
  | Or (phi1, phi2) ->
      print_char '(';
      print_formula phi1 renames;
      print_string " ∨ ";
      print_formula phi2 renames;
      print_char ')'

and print_comp_expr (e : F.comp_expr) (renames) : unit = 
  match e with
  | Attr (i) -> print_attribute_id i renames
  | Data (d) -> print_string (Data.string_of_data d)

and print_comp (op : F.comp) : unit = print_string (
  match op with
  | Eq -> " = "
  | Diff -> " != "
  | Inf -> " < "
  | InfEq -> " <= "
  | Sup -> " > "
  | SupEq -> " >= "
)

and print_attribute_id (i : OPTI.attribute_id) renames =
  let (t, a) = List.assoc i renames in
  print_string (t ^ "." ^ a)

and print_attribute_list (l : OPTI.attribute_id list) renames =
  let rec aux al =
    match al with
    | [] -> print_string ")"
    | a :: [] -> print_attribute_id a renames; print_string ")"
    | a :: tl -> print_attribute_id a renames; print_string ", "; aux tl
  in print_string "("; aux l

(* --------------------------------------------------------------------------------------------- *)
(* --------------------------------------------------------------------------------------------- *)
(* ----------                            Executing a query                            ---------- *)
(* --------------------------------------------------------------------------------------------- *)
(* --------------------------------------------------------------------------------------------- *)

let exec (pq : Compil.query) (debug : bool) : Index.table = 
  let (rename_id_map, refined_query) = transform pq in
  let optimized_query = OPTI.projection_descent (OPTI.select_descent refined_query) in
  let eval_plan = OPTI.find_plan optimized_query 5. in  
  (* Renaming the displayed attributes *)
  let (printed_attributes_map, result) = OPTI.run (fst eval_plan) in
  let (renames, ids) = List.split rename_id_map in
  let id_rename_map = List.combine ids renames in
  List.iter (fun (i, a) -> Index.update_attribute_type_display_name (Index.get_attribute_type a) (snd (List.assoc i id_rename_map))) printed_attributes_map;
  if debug then (
    print_endline "----- query before optimisation -----";
    pretty_print_query refined_query id_rename_map;
    print_endline "---------- optimised query ----------";
    pretty_print_query optimized_query id_rename_map;
    print_endline "-------------------------------------")
  else ();
  result

let _print_test (pq: Compil.query) : unit = 
  let (rename_id_map, refined_query) = transform pq in
  let optimized_query = OPTI.remap_join_attribute (OPTI.projection_descent (OPTI.select_descent refined_query)) in
  let (renames, ids) = List.split rename_id_map in
  let id_rename_map = List.combine ids renames in
  let cost =  OPTI.plan_complexity (fst (OPTI.find_plan optimized_query 1.)) in
    print_endline "----- query before optimisation -----";
    pretty_print_query refined_query id_rename_map;
    print_endline "---------- optimised query ----------";
    pretty_print_query optimized_query id_rename_map;
    print_endline "-------------------------------------";
    print_string "estimated cost of the query: "; print_int cost; print_string " [unit]\n\n\n"
