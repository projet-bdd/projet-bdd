(** Create three test table *)
let create_test_table () =
  (* print_endline "in create_test_table"; *)
  let open Index in
  let att_typs_R = [create_attribute_type "id" TINTEGER [PRIMARY_KEY]; create_attribute_type "a" TINTEGER []] in
  let att_typs_S = [create_attribute_type "id" TINTEGER [PRIMARY_KEY]; create_attribute_type "b" TINTEGER []] in
  let att_typs_T = [create_attribute_type "id" TINTEGER [PRIMARY_KEY]; create_attribute_type "c" TINTEGER []; create_attribute_type "d" TINTEGER []] in
  let att_typs_U = [create_attribute_type "id" TINTEGER [PRIMARY_KEY]; create_attribute_type "e" TINTEGER []] in
  create_table "R" att_typs_R;
  create_table "S" att_typs_S;
  create_table "T" att_typs_T;
  create_table "U" att_typs_U


let fill_table () =
  (* print_endline "in fill_table"; *)
  let open Index in
  let open Data in
  let r = get_table "R" in
  let s = get_table "S" in
  let t = get_table "T" in
  let u = get_table "U" in
  if (Index.table_meta_info r).n_tuples = 0 then (
    insert_row r [|integer 0l; integer 0l|];
    insert_row r [|integer 1l; integer 10l|];
    insert_row r [|integer 2l; integer 20l|]; 
  );

  if (Index.table_meta_info s).n_tuples = 0 then (
    insert_row s [|integer 0l; integer 0l|];
    insert_row s [|integer 1l; integer 100l|];
    insert_row s [|integer 2l; integer 200l|];
  );

  if (Index.table_meta_info t).n_tuples = 0 then (
    insert_row t [|integer 0l; integer 0l; integer 100l|];
    insert_row t [|integer 1l; integer 1000l; integer 200l|];
    insert_row t [|integer 2l; integer 2000l; integer 100l|]; 
  );

  if (Index.table_meta_info u).n_tuples = 0 then (
    insert_row u [|integer 0l; integer 200l|];
    insert_row u [|integer 1l; integer 0l|];
    insert_row u [|integer 2l; integer 100l|]; 
  )


type query = Compil.query
let query_join_cnf : query = 
  JOIN (
    RELATION "R",
    JOIN (
      RELATION "T",
      RELATION "S",
      Or (
        And (
          Eq (("T","id"), ("S","id")),
          Or (
            Eq (("T","c"), ("S","b")),
            Eq (("T","d"), ("S","b"))
          )
        ),
        Or (
          And (
            Eq (("T","c"),("S","b")),
            Eq (("T","d"),("S","b"))
          ),
          Eq (("T","c"), ("T","d"))
        )
      )
    ),
    Eq (("R","id"), ("T","id"))
  )


let query_cross_restrict_test : query =
  SELECT (
    CROSS (
      UNION (
        RELATION "T",
        RELATION "U"
      ),
      UNION (
        RELATION "S",
        RELATION "R"
      )
    ),
    And (
      Eq (("T","id"), ("S","id")),
      Or (
        Eq (("T","c"), ("S","b")),
        Eq (("T","d"), ("S","b"))
      )
    )
  )

let query_proj_test : query =
  PROJ (
    JOIN (
      RELATION "R",
      SELECT (
        CROSS (
          RELATION "T",
          RELATION "S"
        ),
        Eq (("T","id"),("S","id"))
      ),
      Eq (("R","id"),("T","id"))
    ),
    [("R","a")]
  )

let query_big_test : query =
  SELECT (
    CROSS (
      PROJ (
    JOIN (
    RELATION "R",
    JOIN (
      RELATION "T",
      RELATION "S",
      Or (
        And (
          Eq (("T","id"), ("S","id")),
          Or (
            Eq (("T","c"), ("S","b")),
            Eq (("T","d"), ("S","b"))
          )
        ),
        Or (
          And (
            Eq (("T","c"),("S","b")),
            Eq (("T","d"),("S","b"))
          ),
          Eq (("T","c"), ("T","d"))
        )
      )
    ),
    Eq (("R","id"), ("T","id"))
  ),
    [("R","id")]
  ),
      UNION (
        RELATION "S",
        RELATION "R"
      )
    ),
    And (
      Eq (("T","id"), ("S","id")),
      Or (
        Eq (("T","c"), ("S","b")),
        Eq (("T","d"), ("S","b"))
      )
    )
  )

let tests : query list = [query_join_cnf; query_cross_restrict_test; query_proj_test; query_big_test]

let rec run_tests (l: query list) : unit =
  match l with
  | [] -> ()
  | h::t -> ignore (Compil_query._print_test h); run_tests t


let () = create_test_table () ; fill_table () ; run_tests tests