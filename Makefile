DUNE_PARAMS = --profile release

all:
	rm -f sgbd
	dune build bin $(DUNE_PARAMS)
	mv _build/default/bin/sgbd.exe sgbd

doc:
	dune build @doc-private $(DUNE_PARAMS)
	mv _build/default/_doc/_html/ _doc/

test:
	dune runtest $(DUNE_PARAMS)

algo:
	dune build algo $(DUNE_PARAMS)

index:
	dune build index $(DUNE_PARAMS)

compil:
	dune build compil $(DUNE_PARAMS)

optimisation:
	dune build optimisation $(DUNE_PARAMS)

clean:
	dune clean
	rm -f sgbd metadata *.tbl *.tmp *.html *.css *.bptree
	rm -rf _doc/

.PHONY: all algo index compil optimisation clean
